<?php
/**
 * Copyright (c) 2014, 2018 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Wayne Beaton (Eclipse Foundation)- initial API and implementation
 *    Christopher Guindon (Eclipse Foundation) - Re-implementation for the new solstice theme
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");

$App = new App();
$App->PageRSS = "/projects/reviews-rss.php";
$Theme = $App->getThemeClass();

$pageTitle = "Eclipse Membership";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("eclipse membership, become a member, membership faq, membership expire");
$Theme->setTitle("Eclipse Membership Benefits");

$hide_tips = TRUE;
$hide_faqs = TRUE;
$show_corporate_sponsors_block = TRUE;
$show_membership_contact = TRUE;

ob_start();
?>
<div class="jumbotron featured-jumbotron featured-jumbotron-dark featured-jumbotron-astro margin-bottom-0">
  <div class="container">
    <div class="row">
      <div class="col-md-20 col-md-offset-2 col-sm-18 col-sm-offset-3">
        <h1 class="featured-jumbotron-headline"><?php print $pageTitle; ?></h1>
        <p class="featured-jumbotron-subtitle">Supported by our member organizations, the Eclipse Foundation provides our community with Intellectual Property, Mentorship, Marketing, Event and IT Services.</p>
        <ul class="list-inline margin-top-30">
          <li><a class="btn btn-primary" href="https://accounts.eclipse.org/contact/membership">Contact Us About Membership</a></li>
          <li><a class="btn btn-primary" href="https://eclipse.org/membership/exploreMembership.php">Explore Our Members</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<?php
$extra_header_html = ob_get_clean();
$Theme->setExtraHeaderHtml($extra_header_html);

// Place your html content in a file called content/en_pagename.php
ob_start();
include("content/en_" . $App->getScriptName());
$html = ob_get_clean();
$Theme->setHtml($html);
$Theme->setAttributes('header-wrapper', 'header-default-bg-img');
$Theme->removeAttributes('breadcrumbs', 'breadcrumbs-default-margin');
$App->AddExtraJSFooter('<script src="/membership/js/membership-index.js"></script>');
$Theme->setAttributes('main', 'background-white');
$Theme->generatePage();
