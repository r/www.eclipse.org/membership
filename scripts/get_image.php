<?php
/*******************************************************************************
* Copyright (c) 2006-2013 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
*
* Contributors:
*    (Eclipse Foundation)- initial API and implementation
*******************************************************************************/

require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
$App = new App();

$_input_id 		= $App->getHTTPParameter('id', 'GET');
$_input_size 	= $App->getHTTPParameter('size', 'GET');
$is_error = false;

if(!preg_match('/^[0-9]+$/', $_input_id)) {
	$is_error = true;
}

$validSizes = array("large", "small");
if(!in_array($_input_size, $validSizes)) {
	$is_error = true;
}

if(!$is_error) {
	$query = "SELECT ". $_input_size . "_logo, ". $_input_size . "_mime FROM OrganizationInformation WHERE OrganizationID = " . $App->sqlSanitize($_input_id);
	$result = $App->eclipse_sql($query);
	if ($imageIterator = mysql_fetch_array($result)) {
		$imageData = $imageIterator[0];
		$imageMime = $imageIterator[1];

		header("Content-type: " . $imageMime . "\n");
		echo $imageData;
	} else {
		header("Location:https://www.eclipse.org/membership/images/eclipse-mp-member-144x69.png");
	}
}
else {
	header("Location:https://www.eclipse.org/membership/images/eclipse-mp-member-144x69.png");
}
?>
