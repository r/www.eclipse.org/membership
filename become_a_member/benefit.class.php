<?php
class benefit {

	var $name;					// Just a short 50 character heading summarizing benefit
	var $description;			// The elaborate description
	var $how_to_engage;			// HTML description on how to leverage this benefit if you're a member.  "click here"
	var $tags = array(); 	// Used for Categorizing

	function addTags() {
		//no need to do checking as this code is limited use
		$args = func_get_args();
		$this->tags = array_merge($this->tags, $args);
	}

}

