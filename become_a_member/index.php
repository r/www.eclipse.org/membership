<?php
/*******************************************************************************
 * Copyright (c) 2020 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation)
 *    Denis Roy (Eclipse Foundation)
 *******************************************************************************/

header('Location: /membership/#tab-membership', TRUE, 301);
exit;