<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation) - Initial implementation
 *******************************************************************************/

$table = array();
$header = array(
  'Rights',
  'Strategic',
  'Enterprise Member',
  'Solutions Member',
  'Committer',
  'Associate',
);

foreach ($header as $value) {
  $table['header'][] = array(
    'content' => $value,
    'tooltip' => '',
  );
}

$table[] = array(
  array(
    'content' => 'Member of Board of Directors',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Appoints designate to <a href="/org/foundation/directors.php">Board of Directors</a>.'
  ),
  array(
    'content' => '',
    'tooltip' => ''
  ),
  array(
    'content' => '',
    'tooltip' => ''
  ),
  array(
    'content' => '',
    'tooltip' => ''
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
);

$table[] = array(
  array(
    'content' => 'Participate in <a href="/org/industry-workgroups/industry_wg_process.php">Industry Working Groups</a>',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Able to participate/lead IWGs',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Able to participate/lead IWGs',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Able to participate only in IWGs',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Able to participate only in IWGs',
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
);

$table[] = array(
  array(
    'content' => 'Enhanced access to IP Due Diligence Data',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Access for up to 3 users.',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Access for up to 3 users.',
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Access committer as part of committer duties.',
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
);

$table[] = array(
  array(
    'content' => 'Represented on Board of Directors',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Appoints designate to <a href="/org/foundation/directors.php">Board of Directors</a>.',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Board Representative</a>.',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Board Representative</a>.',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Board Representative</a>.',
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
);

$table[] = array(
  array(
    'content' => 'Voting member of Eclipse',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' =>'Where required by the bylaws, some items may need a vote during a <a href="/org/foundation/minutes.php">Members Meeting</a>.  Multiple employees votes are aggregated into a single vote.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' =>'Where required by the bylaws, some items may need a vote during a <a href="/org/foundation/minutes.php">Members Meeting</a>.  Multiple employees votes are aggregated into a single vote.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' =>'Where required by the bylaws, some items may need a vote during a <a href="/org/foundation/minutes.php">Members Meeting</a>.  Multiple employees votes are aggregated into a single vote.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' =>'Where required by the bylaws, some items may need a vote during a <a href="/org/foundation/minutes.php">Members Meeting</a>.  Multiple employees votes are aggregated into a single vote.'
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
);

$table[] = array(
  array(
    'content' => 'Participate in Project Reviews',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Any staff from a Stratgeic member can participate in the <a href="/projects/">Project processes</a>.  Staff from your organization can sign up to automatically receive notice of upcoming reviews, meetings, proposals, etc.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Any staff from an Enterprise member can participate in the <a href="/projects/">Project processes</a>.  Staff from your organization can sign up to automatically receive notice of upcoming reviews, meetings, proposals, etc.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Any staff from a Solutions member can participate in the <a href="/projects/">Project processes</a>.  Staff from your organization can sign up to automatically receive notice of upcoming reviews, meetings, proposals, etc.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Any Committer member can participate in the <a href="/projects/">Project processes</a>.  Staff from your organization can sign up to automatically receive notice of upcoming reviews, meetings, proposals, etc.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Any staff from an Associate member can participate in the <a href="/projects/">Project processes</a>.  Staff from your organization can sign up to automatically receive notice of upcoming reviews, meetings, proposals, etc.',
  ),
);

$table[] = array(
  array(
    'content' => 'Participate in project creation reviews',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Any staff from a Stratgeic member can participate in the <a href="/projects/">Project processes</a>.  Staff from your organization can sign up to automatically receive notice of upcoming reviews, meetings, proposals, etc.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Any staff from an Enterprise member can participate in the <a href="/projects/">Project processes</a>.  Staff from your organization can sign up to automatically receive notice of upcoming reviews, meetings, proposals, etc.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Any staff from a Solutions member can participate in the <a href="/projects/">Project processes</a>.  Staff from your organization can sign up to automatically receive notice of upcoming reviews, meetings, proposals, etc.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Any Committer member can participate in the <a href="/projects/">Project processes</a>.  Staff from your organization can sign up to automatically receive notice of upcoming reviews, meetings, proposals, etc.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Any staff from an Associate member can participate in the <a href="/projects/">Project processes</a>.  Staff from your organization can sign up to automatically receive notice of upcoming reviews, meetings, proposals, etc.',
  ),
);

$table[] = array(
  array(
    'content' => 'Participate in Project IP Discussions',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Any staff from a Stratgeic member can participate in the <a href="/projects/">Project processes</a>.  Staff from your organization can sign up to automatically receive notice of upcoming reviews, meetings, proposals, etc.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Any staff from an Enterprise member can participate in the <a href="/projects/">Project processes</a>.  Staff from your organization can sign up to automatically receive notice of upcoming reviews, meetings, proposals, etc.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Any staff from a Solutions member can participate in the <a href="/projects/">Project processes</a>.  Staff from your organization can sign up to automatically receive notice of upcoming reviews, meetings, proposals, etc.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Any Committer member can participate in the <a href="/projects/">Project processes</a>.  Staff from your organization can sign up to automatically receive notice of upcoming reviews, meetings, proposals, etc.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Any staff from an Associate member can participate in the <a href="/projects/">Project processes</a>.  Staff from your organization can sign up to automatically receive notice of upcoming reviews, meetings, proposals, etc.',
  ),
);

$table[] = array(
  array(
    'content' => 'Support EMO IP due dilligence',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Any staff from a Stratgeic member can participate in the <a href="/projects/">Project processes</a>.  Staff from your organization can sign up to automatically receive notice of upcoming reviews, meetings, proposals, etc.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Any staff from an Enterprise member can participate in the <a href="/projects/">Project processes</a>.  Staff from your organization can sign up to automatically receive notice of upcoming reviews, meetings, proposals, etc.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Any staff from a Solutions member can participate in the <a href="/projects/">Project processes</a>.  Staff from your organization can sign up to automatically receive notice of upcoming reviews, meetings, proposals, etc.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Any Committer member can participate in the <a href="/projects/">Project processes</a>.  Staff from your organization can sign up to automatically receive notice of upcoming reviews, meetings, proposals, etc.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Any staff from an Associate member can participate in the <a href="/projects/">Project processes</a>.  Staff from your organization can sign up to automatically receive notice of upcoming reviews, meetings, proposals, etc.',
  ),
);

$table[] = array(
  array(
    'content' => 'Use of <a href="http://eclipse.org/artwork">Eclipse Foundation Member Logo</a>',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Strategic members are able to fully market themselves as such.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Enterprise members are able to fully market themselves as such.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Solutions members are able to fully market themselves as such.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Committer members are able to fully market themselves as such.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Associate members are able to fully market themselves as such.'
  ),
);

$table[] = array(
  array(
    'content' => 'Join Councils',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Able to apoint representative to <a href="/org/foundation/council.php">Eclipse Foundation Councils</a>.'
  ),
  array(
    'content' => '',
    'tooltip' => ''
  ),
  array(
    'content' => '',
    'tooltip' => ''
  ),
  array(
    'content' => '',
    'tooltip' => ''
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
);

$table[] = array(
  array(
    'content' => 'Approves Top Level Projects',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Via <a href="/org/foundation/directors.php">Board of Directors</a>.'
  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Board Representative</a>.'
  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Board Representative</a>.'
  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Committer Board Representative</a>.'
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
);

$table[] = array(
  array(
    'content' => 'Approves Release Roadmap',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Via <a href="/org/foundation/directors.php">Board of Directors</a>.'

  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Board Representative</a>.'
  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Committer Board Representative</a>.'
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
);

$table[] = array(
  array(
    'content' => 'Hires and fires Executive Director',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Via <a href="/org/foundation/directors.php">Board of Directors</a>.'

  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Board Representative</a>.'
  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Committer Board Representative</a>.'
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
);

$table[] = array(
  array(
    'content' => 'Approves Eclipse Foundation Org. Budget',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Via <a href="/org/foundation/directors.php">Board of Directors</a>.'

  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Board Representative</a>.'
  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Committer Board Representative</a>.'
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
);

$table[] = array(
  array(
    'content' => 'Sets membership dues',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Via <a href="/org/foundation/directors.php">Board of Directors</a>.'

  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Board Representative</a>.'
  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Board Representative</a>.'
  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Committer Board Representative</a>.',
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
);

$table[] = array(
  array(
    'content' => 'Approves changes to Development Process',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Via <a href="/org/foundation/directors.php">Board of Directors</a>.'

  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Board Representative</a>.'
  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Board Representative</a>.'
  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Committer Board Representative</a>.',
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
);

$table[] = array(
  array(
    'content' => 'Approves formal affiliations',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Via <a href="/org/foundation/directors.php">Board of Directors</a>.'

  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Board Representative</a>.'
  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Board Representative</a>.'
  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Committer Board Representative</a>.',
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
);

$table[] = array(
  array(
    'content' => 'Selects standards organizations',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Via <a href="/org/foundation/directors.php">Board of Directors</a>.'

  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Board Representative</a>.'
  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Board Representative</a>.'
  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Committer Board Representative</a>.',
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
);

$table[] = array(
  array(
    'content' => 'Governs Eclipse Public License',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Via <a href="/org/foundation/directors.php">Board of Directors</a>.'

  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Board Representative</a>.'
  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Board Representative</a>.'
  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Committer Board Representative</a>.',
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
);

$table[] = array(
  array(
    'content' => 'Selects outside counsel, IP and antitrust policy',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Via <a href="/org/foundation/directors.php">Board of Directors</a>.'

  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Board Representative</a>.'
  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Board Representative</a>.'
  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Committer Board Representative</a>.',
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
);

$table[] = array(
  array(
    'content' => 'Terminates and reinstates Members',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Via <a href="/org/foundation/directors.php">Board of Directors</a>.'

  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Board Representative</a>.'
  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Board Representative</a>.'
  ),
  array(
    'content' => '<i class="fa fa-star"></i>',
    'tooltip' => 'Via <a href="/org/elections/">Elected Committer Board Representative</a>.',
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
);

$table[] = array(
  array(
    'content' => 'Approves Bylaws and Membership Agreement',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Changes require vote during a <a href="/org/foundation/minutes.php">Members Meeting</a>.  Multiple employees votes are aggregated into a single vote.'

  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Changes require vote during a <a href="/org/foundation/minutes.php">Members Meeting</a>. Votes from multiple employees are aggregated into one vote.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Changes require vote during a <a href="/org/foundation/minutes.php">Members Meeting</a>. Votes from multiple employees are aggregated into one vote.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Changes require vote during a <a href="/org/foundation/minutes.php">Members Meeting</a>.  Committer votes from a single organization are aggregated into one vote.'
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
);

$table[] = array(
  array(
    'content' => 'Approves Changes to Eclipse Foundation Name',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Changes require vote during a <a href="/org/foundation/minutes.php">Members Meeting</a>.  Multiple employees votes are aggregated into a single vote.'

  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Changes require vote during a <a href="/org/foundation/minutes.php">Members Meeting</a>. Votes from multiple employees are aggregated into one vote.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Changes require vote during a <a href="/org/foundation/minutes.php">Members Meeting</a>. Votes from multiple employees are aggregated into one vote.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Changes require vote during a <a href="/org/foundation/minutes.php">Members Meeting</a>.  Committer votes from a single organization are aggregated into one vote.'
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
);

$table[] = array(
  array(
    'content' => 'Approves actions affecting member liabilities',
    'tooltip' => '',
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Changes require vote during a <a href="/org/foundation/minutes.php">Members Meeting</a>.  Multiple employees votes are aggregated into a single vote.'

  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Changes require vote during a <a href="/org/foundation/minutes.php">Members Meeting</a>. Votes from multiple employees are aggregated into one vote.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Changes require vote during a <a href="/org/foundation/minutes.php">Members Meeting</a>. Votes from multiple employees are aggregated into one vote.'
  ),
  array(
    'content' => '<i class="fa fa-check"></i>',
    'tooltip' => 'Changes require vote during a <a href="/org/foundation/minutes.php">Members Meeting</a>.  Committer votes from a single organization are aggregated into one vote.'
  ),
  array(
    'content' => '',
    'tooltip' => '',
  ),
);

function printRow($cell, $header, $type) {
  $return = '';
  $cell_id = 0;
  foreach ($cell as $value) {
  	$class = "";
  	if($cell_id) {
  	  $class = ' class="text-center"';
    }
    $return .= '<' . $type . $class . '>';
    if (!empty($value['tooltip'])) {
      $return .= '<span style="cursor:pointer" data-html="true" data-toggle="popover" title="' . $header[$cell_id] . '" data-content="' . htmlentities($value['tooltip']) . '">' . $value['content'] . '</span>';
    }
    else{
      $return .= $value['content'];
    }
    $return .= '</' . $type . '>';
    $cell_id++;
  }
  return $return;
}
?>
<div class="col-md-14 col-lg-16">
	<h1><?=$pageTitle?></h1>
	<p class="margin-bottom-25 padding-bottom-15">Click on a (<i class="fa fa-check"></i>) or (<i class="fa fa-star"></i>) for more details.</p>
	<table class="table table-striped table-solstice">
	<?php foreach ($table as $key => $row) :?>
	  <tr>
	    <?php if ($key === 'header'): ?>
	      <?php print printRow($row, $header, 'th');?>
	    <?php else:?>
	      <?php print printRow($row, $header, 'td');?>
	    <?php endif;?>
	  </tr>
	<?php endforeach; ?>
	</table>
</div>
<style type="text/css">
  .table-solstice i{
    font-size:20px;
    color:#858585;
  }
</style>
<?php require_once('../content/en_sidebar.php');?>