<?php
/**
 * *****************************************************************************
 * Copyright (c) 2014,2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Christopher Guindon (Eclipse Foundation) - Initial implementation
 * *****************************************************************************
 */
?>
<div class="col-md-14 col-lg-16">
  <h1><?=$pageTitle?></h1>
  <p>Each organization has their own unique reasons for joining the
    Eclipse Foundation. Some see it as an opportunity to network, learn
    and work with other organizations who are making strategic use of
    Open Source. Others join because of the many marketing benefits
    available. We invite you to explore the benefits of membership on
    this page to learn how your organization might benefit. If you have
    questions about any of these benefits, or would like to suggest some
    that may be missing - please <a href="mailto:membership@eclipse.org">email us.</a>
  </p>

  <p>Membership in the Eclipse Foundation allows organizations to
    leverage the resources of the community and achieve a higher return
    on their investment in Eclipse products and services. Eclipse
    members are offered a unique opportunity to understand the trends
    and directions of the Eclipse technology and participate in
    marketing programs that drive potential users to your offerings.</p>

  <p>There are three areas to discover the many benefits of membership:</p>

  <ul>
    <li>A high level summary of benefits, by membership level, can be <a
      href="./benefitsByMembership.php"> found here</a>.</li>
    <li>The Eclipse Bylaws describe certain rights <a
      href="memberRights.php">summarized here.</a></li>
    <li>The Eclipse Foundation runs a number of programs that change
      over time to benefit our membership. A summary of some of those
      programs is described in the table below.</li>
  </ul>

  <br/>

  <!-- Tab panes -->
  <?php print implode('', $variables['tab_pane']);?>
</div>
<?php require_once('../content/en_sidebar.php');?>
