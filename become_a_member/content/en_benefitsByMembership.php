<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<div class="col-md-14 col-lg-16">
  <h1>Overview of Membership Benefits by Membership Class</h1>
  <p>
  The Eclipse Foundation is frequently running new programs and initiatives for the benefit of our members.
  Below is a high level overview of what kinds of programs and initiatives are available to the various membership classes.
  </p>

  <div class="sideitem background-grey margin-top-25">
    <h2>Associate Members</h2>
    <div class="content">
      <ul>
      <li>Access to Information (mail lists, attend members only meetings)</li>
      <li>Show of support (logo exchange, etc)</li>
    </ul>
  </div>
</div>

  <div class="sideitem background-grey">
    <h2>Solutions Members</h2>
    <div class="content">
      <ul>
        <li>Associate Member Benefits</li>
        <li>Voting rights for board representation, project reviews and amendments to Membership Agreement and Bylaws</li>
        <li>Significant discounts towards sponsorship and attendance of Eclipse Foundation run events such as EclipseCon</li>
        <li>Access to Eclipse Foundation marketing and advertising programs.</li>
        <li>Ability to participate in Industry Working Groups</li>
      </ul>
    </div>
  </div>

  <div class="sideitem background-grey">
    <h2>Enterprise Members</h2>
    <div class="content">
      <ul>
        <li>All Solutions Member Benefits</li>
        <li>Intellectual Property Analysis and Reporting</li>
        <li>Help launching Open Source initiative(s)</li>
      </ul>
    </div>
  </div>

  <div class="sideitem background-grey">
    <h2>Strategic Members</h2>
      <div class="content">
      <ul>
        <li>All Enterprise Member Benefits</li>
        <li>Seat on Board of Directors of Eclipse Foundation</li>
        <li>Seat on Foundation Councils</li>
        <li>Access to a number of Strategic Members Only Programs such as banner and logo promotion on eclipse.org and eclipse.org/downloads</li>
      </ul>
    </div>
  </div>
</div>

<?php require_once('../content/en_sidebar.php');?>