<?php
/*******************************************************************************
 * Copyright (c) 2005,2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation)
 *    Denis Roy (Eclipse Foundation)
 *******************************************************************************/

  require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
  require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
  require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
  $App = new App();
  $Nav = new Nav();
  $Menu = new Menu();
  include($App->getProjectCommon());

  # Begin: page-specific settings.  Change these.
  $pageTitle = "Membership Rights";
  $pageKeywords = "membership rights";
  $pageAuthor = "Christopher Guindon";

  // Custom theme variables
  $variables = array();

  // Place your html content in a file called content/en_pagename.php
  ob_start();
  include("content/en_" . $App->getScriptName());
  $html = ob_get_clean();

  require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/breadcrumbs.class.php");
  $Breadcrumb = new Breadcrumb();
  # remove last crumb since it represents this _projectCommon page.
  $Breadcrumb->removeCrumb($Breadcrumb->getCrumbCount() -1);
  $Breadcrumb->addCrumb("Become a member", "/membership/become_a_member/", "_self");
  $Breadcrumb->addCrumb($pageTitle, NULL, NULL);

  # Generate the web page
  $App->AddExtraJSFooter('<script type="text/javascript">$(document).ready(function() {$(\'[data-toggle="popover"]\').popover();$(\'[data-toggle="popover"]\').click(function () {$(\'[data-toggle="popover"]\').not(this).popover(\'hide\');});});</script>');
  $App->generatePage($theme, $Menu, NULL, $pageAuthor, $pageKeywords, $pageTitle, $html, $Breadcrumb);
