module.exports = function(grunt) {
    // Initializing the configuration object
    grunt.initConfig({
        // Task configuration
        less: {
            development: {
                options: {
                    compress: true,
                    // minifying the result
                },
                files: {
                    // compiling styles.less into styles.css
                    "./css/becomemember.min.css": "./bower_components/solstice-assets/stylesheets/eclipse_org/becomemember/styles.less",
                }
            }
        },
        concat: {
            options: {
                separator: ';',
            },
        },
        watch: {
            less: {
                files: [
                  './bower_components/solstice-assets/stylesheets/eclipse_org/becomemember/*.less',
                  './bower_components/solstice-assets/stylesheets/eclipse_org/becomemember/**/*.less'
                  ],
                // watched files
                tasks: ['less'],
                // tasks to run
            },
        }
    });
    // Plugin loading
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    // Task definition
    grunt.registerTask('default', ['watch']);
};