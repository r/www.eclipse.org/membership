<?php
/*******************************************************************************
 * Copyright (c) 2014-2021 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Denis Roy (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/

  require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
  require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
  require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");

  $App   = new App();
  $Nav  = new Nav();
  $Menu = new Menu();

  include($App->getProjectCommon());

  # Begin: page-specific settings.  Change these.
  $pageTitle     = "Membership Benefits";
  $pageKeywords  = "Membership benefits";
  $pageAuthor    = "Christopher Guindon";

  include("benefit.class.php");
  $benefits = array();

  $ben = new benefit();
  $ben->name =  'Advertising space on eclipse.org';
  $ben->description = 'As an exclusive benefit only to members, we provide
advertising space on many of our eclipse.org web properties.
These ads are prominently displayed on our properties that are
visited by millions of developers who visit our site, and our
advertising solutions are very cost effective.  In addition,
we offer free promotion as a benefit for our Strategic
members on the eclipse.org home page.
Please contact our <a href="https://eclipse-5413615.hs-sites.com/ad-prospectus-form">membership team</a> for more information.';
  $ben->how_to_engage = "";
  $ben->addTags("Marketing");
  array_push($benefits, $ben);

  $ben = new benefit();
  $ben->name = "Member Designation on Eclipse Marketplace";
  $ben->description = 'Strategic, Enterprise and Solution members will receive
  special designation for their listing on <a href="http://marketplace.eclipse.org/">Eclipse Marketplace</a>.
  This designation means their listing will be accessible from the
  Eclipse Marketplace Client, making it easier for Eclipse users to install
  member solutions directly into Eclipse.';
    $ben->how_to_engage = "";
  $ben->addTags("Marketing");
  array_push($benefits, $ben);

  $ben = new benefit();
  $ben->name =  'Promote special offers to other members in Foundation Newsletter';
  $ben->description = 'Have a special offer you want to offer other members? Place a short ad in the Foundation Newsletters!';
  $ben->how_to_engage = "";
  $ben->addTags("Networking and Learning");
  array_push($benefits, $ben);

  $ben = new benefit();
  $ben->name =  'Organizational Membership page Eclipse.org Web Site';
  $ben->description = 'Links posted on Eclipse.org are heavily trafficed and well indexed.  Each member is provided the opportunity to
          post their company information, product information and logo on their own <a href="../exploreMembership.php">membership page</a>.';
  $ben->how_to_engage = "";
  $ben->addTags("Marketing");
  array_push($benefits, $ben);

  $ben = new benefit();
  $ben->name =  'News Stories ';
  $ben->description = 'News links posted on Eclipse.org are heavily trafficked and read tens of thousands of times
         each. The <a href="http://eclipse.org">Eclipse.org</a> web site highlights
         members&rsquo; news related stories in the Community section. Interested in submitting a news story, please send an email to <a href="mailto:news@eclipse.org?subject=Request to Post Member News&body=Include URL to news article you would like posted.">
         news@eclipse.org</a>.';
  $ben->how_to_engage = "";
  $ben->addTags("Marketing");
  array_push($benefits, $ben);

  $ben = new benefit();
  $ben->name = "Support for Press releases";
  $ben->description = 'Eclipse Foundation will provide press assistance and quotes for new member press releases and ongoing member product announcements. Please email <a href="mailto:membership@eclipse.org">membership@eclipse.org</a>.';
  $ben->how_to_engage = "";
  $ben->addTags("Marketing");
  array_push($benefits, $ben);

  $ben = new benefit();
  $ben->name = "Access to Member Marketing mailing list";
  $ben->description = 'Mailing lists are a key method that the foundation uses to communicate with the
    membership.  Please note some of the <a href="http://www.eclipse.org/mail/index_topic.php"> mailing lists</a> that
    are available.  As members, you will certainly want to sign up for the
    <strong>eclipse.org-member-marketing</strong> mailing list. This list will be useful not only to people marketing Eclipse
    based products and services, but also Engineering managers, directors and
    executives interested in keeping abreast of activities in the Eclipse ecosystem.
    For more information on sending to the member marketing list, please send an email to <a href="mailto:membership@eclipse.org">membership@eclipse.org</a>.';
    $ben->how_to_engage = "";
  $ben->addTags("Marketing");
  array_push($benefits, $ben);

  $ben = new benefit();
  $ben->name = "Special Promotional Activities";
  $ben->description = 'The size and popularity of Eclipse community presents members unique promotional
      opportunities, ex. advertising supplements, newsletter sponsorships, etc.  Some promotional activities will be limited
      to Strategic, Solutions and Enterprise Members depending on the context.
      For more information on sending to the member marketing list, please send an email to <a href="mailto:membership@eclipse.org">membership@eclipse.org</a>.';
    $ben->how_to_engage = "";
  $ben->addTags("Marketing");
  array_push($benefits, $ben);

  $ben = new benefit();
  $ben->name = "EclipseCon Discounts";
  $ben->description = 'The Eclipse community host three annual conferences in North American and Europe.
      This is a perfect opportunity
      to meet potential partners and customers. Eclipse members receive
      preferential exhibit space and sponsorship opportunities, show discounts
      and other members only opportunities.';
  $ben->how_to_engage = "";
  $ben->addTags("Networking and Learning");
  array_push($benefits, $ben);

  $ben = new benefit();
  $ben->name = "Members Meetings and Teleconferences";
  $ben->description = 'Eclipse <a href="http://www.eclipse.org/org/foundation/minutes.php">member meetings </a>
      and committee discussions create many opportunities to network with other
      technical, marketing and business development professionals. Members
      discuss and leverage shared resources to solve Member problems and grow
      the Eclipse community.  There are two face-to-face members meets and two teleconference meetings each year, plus
      many other opportunities and events.';
    $ben->how_to_engage = "";
  $ben->addTags("Networking and Learning");
  array_push($benefits, $ben);

  $ben = new benefit();
  $ben->name = "Membership At Large Access";
  $ben->description = 'Members will automatically be subscribed to the Eclipse membership-at-large
    mailing list which contains consolidated information regarding the Eclipse ecosystem including notice of
    upcoming members meetings, project reviews, events and other important information.  It represents a single
    feed of information to keep up with the activities of the Eclipse Foundation.';
    $ben->how_to_engage = "";
  $ben->addTags("Consolidated Information");
  array_push($benefits, $ben);

  $ben = new benefit();
  $ben->name = "Quarterly Director Reports";
  $ben->description = 'Each quarter our members will receive access to important statistics and information
    about the Eclipse Ecosystem.  The
    Executive Directors Report contains a summary of new members, foundation activities and plans, market research and
    other relevant stats, and a general update of the Ecosystem.
    The project update represents a summary of Eclipse project activities including new proposals, release plans
    and other important project information.';
    $ben->how_to_engage = "";
  $ben->addTags("Consolidated Information");
  array_push($benefits, $ben);

  $ben = new benefit();
  $ben->name = "Monthly Member Newsletters";
  $ben->description = 'Each month our members will be emailed the <a href="http://www.eclipse.org/community/newsletter/">Eclipse Member
  Newsletter</a>, which gives  a summary of important news, events and special offers pertaining to members. ';
  $ben->addTags("Consolidated Information");
  array_push($benefits, $ben);

  // Setup tabs.
  $variables = array(
    'content' => array(),
    'tab_pane' => array(),
  );
  foreach ($benefits as $value) {
    foreach($value->tags as $t){
      $variables['content'][$t][] = $value;
    }
  }

  // Setup content.
  foreach ($variables['content'] as $key => $value) {
    $safe_name = strtolower(str_replace(" ", "", $key));
    $tab_pane = '<div class="sideitem background-grey"><h2>' . $key . '</h2><div class="content"><ul class="list-padding">';
    $id = 0;
    foreach ($value as $b) {
      $name = $b->name;
      if (!empty($b->how_to_engage)) {
        $name = '<a href="'.$b->how_to_engage.'">' .$b->name . '</a>';
      }
      $tab_pane .= '<li><h4 class="list-heading">' . $name . '</h4>';
      $tab_pane .= $b->description . '</li>';
      $id++;
    }
    $tab_pane .= '</ul></div></div>';
    $variables['tab_pane'][] = $tab_pane;

  }

  // Place your html content in a file called content/en_pagename.php
  ob_start();
  include("content/en_" . $App->getScriptName());
  $html = ob_get_clean();

  require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/breadcrumbs.class.php");
  $Breadcrumb = new Breadcrumb();
  # remove last crumb since it represents this _projectCommon page.
  $Breadcrumb->removeCrumb($Breadcrumb->getCrumbCount() -1);
  $Breadcrumb->addCrumb("Become a member", "/membership/become_a_member/", "_self");
  $Breadcrumb->addCrumb($pageTitle, NULL, NULL);

  # Generate the web page
  $App->generatePage(NULL, $Menu, NULL, $pageAuthor, $pageKeywords, $pageTitle, $html, $Breadcrumb);
