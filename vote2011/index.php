<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("../_projectCommon.php");    # All on the same line to unclutter the user's desktop'
  
$pageTitle 		= "Eclipse Bylaw Changes 2011";
$pageAuthor = "Andrew Ross";
$pageKeywords = "membership, vote2011";

ob_start();
?>
<div id="maincontent">
<div id="midcolumn">

<h1><?=$pageTitle?></h1>

<p>VOTING CONCLUDED AUGUST 12, 2011 at 4pm EDT. The new Bylaw vote passed. The results were:</p>
<ul>
  <li>YES: 224</li>
  <li>NO: 5</li>
</ul> 

<p>Here are links to the new Bylaw document:</p>
<ul>
<li><a href="/org/documents/2011.06%20Members%20Meeting%20(Bylaws).pdf">Summary of Bylaws changes.</a></li>
<li><a href="/org/documents/2011.05%20Eclipse%20Bylaws_commented.pdf">New Bylaws with change bars and comments.</a></li>
<ul>
</p>

<p>
The current <a href="http://www.eclipse.org/org/documents/Eclipse%20BYLAWS%202008_07_24%20Final.pdf">Bylaws of the Eclipse Foundation </a> 
have served our community well for a number of years. Changes have been proposed to the Bylaws to prepare them for the future and to reflect
current practice. These changes require super-majority (two-thirds plus one) approval of the Membership-at-Large to become effective.
</p>

<p>The By-law changes have been unanimously approved by the board and were outlined in the Annual General Members Meeting on March 21, 2011 
and then Members Meeting on June 21, 2011.</p>

<p>
On JULY 12, 2011 we initiated an electronic vote of the Eclipse Foundation Membership-at-Large to approve the proposed changes. All Member Company
Representatives and Committers should vote on the proposed changes. If you are a Member Company Represenative or Committer Member and did not 
receive your official voting instructions on JULY 12, 2011 please contact <a href="mailto:membership@eclipse.org">membership at eclipse.org</a>.
</p>

<h2>The proposed changes</h2>
<p>
<ul>
<li>Replace the annual Roadmap with an annual community report</li>
<li>Eliminate the Membership Committee.  Make the IP Advisory Committee a Standing Committee</li>
<li>Delete the Requirements Council</li>
<li>Put the Architecture Council in charge of future revisions of the Eclipse Development Process</li>
<li>Delete the requirement for there to be a ratio of Strategic Consumers to Strategic Developers</li>
<li>Remove the requirement that the AGM be in Q1</li>
<li>Allow for flexibility in covering expenses</li>
<li>Sustaining Member board reps need to be a Sustaining Member</li>
<li>Revisit collapsing votes from same member company to a single vote. Proposed solution: One committer one vote
Total number of candidates from the same organization may not exceed one-half (1/2) of the total number of seats 
available for that year’s annual at-large election</li> 
</ul>
</p>


<h2>FAQ</h2>
<p><b>Who do I contact for more information?</b> For email questions, please 
contact <a href="mailto:membership@eclipse.org">membership@eclipse.org</a>. 
If you would like to speak to someone in North America or APAC, please call Andrew Ross at +1 (613) 614-5772. 
In Europe and India, please contact Ralph M&uuml;ller
at +49 (177) 4490460.
</p>


<p><b>How does the voting process work?</b>  All Committer Members and Company Representatives are required to vote. Ballots
have been sent electronically by email. Committer Members are Committers who work for a Member Company or have completed the 
Committer Membership paperwork. Each Company Representative vote counts as one vote. Each group of committers for a Member 
Company also count as a single vote based on a collapsable single vote. Each vote by a Committer who has completed the 
Committer Membership paperwork and is not working for a Member Company will also count as one vote. The voting process
 will be open for 30 days until close of business August 11 , 2011. A super-majority of affirmative votes is required 
 for the changes to become effective.
</p>


<p><b>I do not see any of the benefits of membership noted in the Membership Agreement or Bylaws; how do I find out about those?</b>  Members
all have certain rights and obligations as noted in the Bylaws and Membership Agreement. However, many of the benefits members receive are
from programs run by the Eclipse Foundation - thanks to the support of its membership. You can find out about these benefits on the 
<a href="/membership/become_a_member/benefits.php?member=yes">membership benefits</a> page and some of the 
special programs being run on the <a href="/membership/special_programs">special programs</a> page. New benefits and programs are always being added
and you can find out about those from your Member Newsletters, Members Meetings, and other information feeds.
</p>

</div><!-- midcolumn -->

</div><!-- maincontent -->
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>