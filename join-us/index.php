<?php
/**
 * Copyright (c) 2014, 2018 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Denis Roy (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *    Zhou Fang (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
$App = new App();
$Theme = $App->getThemeClass();

// Begin: page-specific settings.  Change these.
$pageTitle = "Join Us";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("Eclipse membership, Eclipse members, Become an Eclipse member");

// Place your html content in a file called content/en_pagename.php
ob_start();
include("content/en_" . $App->getScriptName());
$html = ob_get_clean();
$Theme->setHtml($html);

// remove breadcrumb margin bottom
$Theme->removeAttributes('breadcrumbs', 'breadcrumbs-default-margin');
$Theme->setAttributes('header-wrapper', 'header-default-bg-img');
$Theme->setAttributes('main', 'background-white');
// Generate the web page
$Theme->generatePage();