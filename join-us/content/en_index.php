<?php

/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation) - initial API and implementation
 *    Zhou Fang (Eclipse Foundation)
 *******************************************************************************/
?>
<h1 class="page-header">Contact us about your organization joining the Eclipse Foundation</h1>
<div class="row">
    <div class="col-sm-7 margin-bottom-20">
        <p>
            If you are looking for more information on all of the benefits of
            Eclipse Foundation memberships, download the <a href="/membership/documents/membership-prospectus.pdf">Membership
                Prospectus</a>.
        </p>
        <p>
            Looking for information on joining as a Committer Member of the Eclipse
            Foundation? <a href="/membership/become_a_member/committer.php">Check
                this out</a>!
        </p>
    </div>
    <div class="col-sm-16 col-sm-offset-1 margin-bottom-20 solstice-block-default solstice-block-white-bg">
        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
        <script>
            hbspt.forms.create({
                portalId: "5413615",
                formId: "3fe3f2f6-1dbb-45f5-83e7-a219dc2e83f9"
            });
        </script>
    </div>
</div>