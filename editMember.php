<?php
/**
 * Copyright (c) 2014, 2018 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - initial API and implementation
 *    Christopher Guindon (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/classes/membership/editMembership.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/breadcrumbs.class.php");

$App = new App();
$Nav = new Nav();
$Theme = $App->getThemeClass();
$EditMembership = New EditMembership();

$Breadcrumb = new Breadcrumb();
$Breadcrumb->removeCrumb($Breadcrumb->getCrumbCount() -1);
$Breadcrumb->addCrumb("Eclipse Membership", "/membership/exploreMembership.php", "_self");
$Breadcrumb->addCrumb($EditMembership->getMemberName(), NULL, NULL);
$Theme->setBreadcrumb($Breadcrumb);

$Theme->setNav($Nav);
$App->preventCaching();

// Begin: page-specific settings.  Change these.
$pageTitle =  "Edit ". $EditMembership->getMemberName() ." Membership Page";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("Edit Eclipse Membership profile page");

ob_start();
print $EditMembership->outputPage();
$html = ob_get_clean();

$Theme->setHtml($html);
$Theme->generatePage();