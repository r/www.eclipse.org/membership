<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("../_projectCommon.php");    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "Eclipse Membership Meetings";
$pageAuthor = "Andrea Ross";
$pageKeywords = "membership, meetings";

ob_start();
?>
<div id="maincontent">
<div id="midcolumn">

<h1><?=$pageTitle?></h1>

<h2>Membership Meeting</h2>

<p>The next Eclipse Foundation Members meeting will take place on Tuesday, October 25th 10:30 CET.</a></p>

<!--<p>The meeting will be broadcast using Google Hangout (the link to it will appear below on this page close to the meeting). A recording will be available in YouTube.</a>.</p>-->

<p>This meeting is meant for Eclipse Foundation Members and Project Committers, however, all are welcome and especially EclipseCon Europe attendees.</p>

<p>It takes place at EclipseCon Europe, at the FMZ in Konferenzraum 3. The FMZ building is across the street from the Forum, which hosts EclipseCon Europe. Ask at the EclipseCon registration desk in the Forum if you need directions.</p>

<h3>Agenda</h3>
<ul>
<li>Welcome - Paul White (VP of Membership, Eclipse Foundation)</li>
<li>Interesting things for projects - Wayne Beaton (Director of Open Source Projects, Eclipse Foundation)</li>
<li>Social Media Strategy - Kat Hirsch (Marketing Specialist, Eclipse Foundation)</li>
<li>Red Hat contributions to Eclipse Projects - Xavier Coulon (Senior Software Engineer, Red Hat)</li>
<li>Science Working group &amp; Diversity @ Eclipse - Tracy Miranda (Co-Founder, Kichwacoders)</li>
<li>AGILE Initiative - Philippe Krief (Director of Research Relations, Eclipse Foundation)</li>
<li>2 years of automated error reporting @ Eclipse - Marcel Bruch (CEO, Codetrails)</li>
<!--<li>IoT working group - Ian Skerrett (VP of Marketing, Eclipse Foundation)</li>-->
<!--<li>Polarsys – Ralph Mueller (Managing Director, Eclipse Foundation Europe)</li>-->
<!--<li>Marketing - Ian Skerrett (VP of Marketing, Eclipse Foundation)</li>-->
<li>Membership &amp; Conferences - Paul White (VP of Membership, Eclipse Foundation)</li>
<li>Executive Director's update - Mike Milinkovich (Eclipse Foundation</li>
  <li>Q&amp;A</li>
</ul>

<!-- <p>A link to the live stream will appear immediately below this text on this page once the meeting starts. If you don't see it, refresh this page in your browser.</p> -->

<p><strong><a href="https://www.youtube.com/watch?v=qdWET_uuKco&feature=youtu.be">Watch the previous meeting</strong></a></p>

<!-- <a href="https://plus.google.com/events/cilk849n4c6qkgituehn9bf6rdc">Watch the previous meeting video</a> -->


<h3>Questions</h3>

<p>This meeting is in person, and so questions are best directed to the speakers in person.</p>

<p>After the meeting, feel free to ask questions via. twitter (#eclipsemm hash tag), IRC (#eclipse channel on freenode), or emailing <a href="mailto:andrea.ross@eclipse.org?subject=member meeting question">Andrea Ross</a></p>

</div><!-- midcolumn -->

</div><!-- maincontent -->
<?php
	# Paste your HTML content between the EOHTML markers!
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage(NULL, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
