<?php
/**
 * Copyright (c) 2019 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<h1><?php print $pageTitle;?></h1>

<p>The Voting period ended on December 12, 2019 @ 4 pm ET with the new Bylaws being ratified.  The new Bylaws will go into effect on January 1, 2020.</p>

<p>The results of the vote were:</p>
<ul>
  <li>YES: 186 (96%)</li>
  <li>NO: 7 (4%)</li>
</ul>

<p>The Eclipse Foundation received approval of its Board of Directors to update the
  Eclipse Foundation&rsquo;s Bylaws at its October 21, 2019 Board meeting.</p>
<ul>
  <li><a href="#proposed_changes">Summary of Bylaws changes</a></li>
  <li><a href="/membership/vote2019/documents/bylaws_proposed.pdf">Proposed Bylaws</a></li>
  <li><a href="/membership/vote2019/documents/bylaws_proposed_annotated.pdf">Proposed Bylaws annotated</a></li>
  <li><a href="https://youtu.be/Vzw4S1RJd4s">2019 Annual Member Meeting Bylaw Presentation (Recorded)</a></li>
</ul>
<p>The current Bylaws of the Eclipse Foundation have served our community well since 2011.
  The proposed changes reflect the broader scope in technologies encompassed by Eclipse projects
  today as new projects push us into new areas and working groups turn the Eclipse Foundation into a
  &ldquo;large tent of open collaboration&rdquo;.</p>
<p>In addition to Board approval, Bylaw changes require a vote of the Membership
  At-Large.  This includes all classes of membership except Associate Members, and does include
  Committer Members.  The approval of a super-majority (two thirds) of the Membership At-Large
  is required for these changes to go into effect. </p>
<p>The election will run through December 12, 2019.  Ballots will be distributed by
  email to all Company Representatives and Committer Members. </p>
<p>We urge all Members to vote to enable these important changes. </p>
<h2 id="proposed_changes">The proposed changes:</h2>
<ul>
  <li>Realign the definition of the Purposes of the Foundation to reflect our current activities.
    This is a key update to recognize the vast portfolio of projects, technologies, and
    specifications hosted by the Foundation  </li>
  <li>More clearly state the rights and benefits of affiliate organizations and clarify membership
    dues related to those affiliates, all with the intent to make clear affiliate organizations are
    welcome and encouraged each to participate</li>
  <li>Remove the requirement for Solutions Members and higher to release a commercial offering, as
    this requirement has caused significant confusion over the years</li>
  <li>Add approval of changes to the Eclipse Foundation Specification Process to require a
    super-majority vote by the Board, thus making it consistent with the Eclipse Development Process</li>
  <li>Remove the requirement for a super-majority of the Board to approve changes to PMC Leadership
    (and thus require only a simple majority)</li>
  <li>Clarify the EMO&rsquo;s relationship with its legal counsel</li>
  <li>Realign the quarterly reporting requirements to members to enable the EMO to more effectively
    disseminate information to members</li>
  <li>Clarify and modernize the roles of the Architecture and Planning Councils</li>
</ul>
<h2>FAQ</h2>
<h4>Q. Why is the Eclipse Foundation making these changes now? </h4>
<p>Our Bylaws have not been updated since 2011.  At that time, it was a very minor
  update. There are several areas of the Bylaws that require clarity and/or simply updating to
  reflect present day reality.</p>
<h4>Q. Why is a vote required to make this change?</h4>
<p>The Foundation&rsquo;s Bylaws explicitly state that any change to the Bylaws must be
  approved by the Board of Directors as well a vote must be held of the existing members.</p>
<h4>Q. Has the Foundation&rsquo;s Board of Directors approved this change?</h4>
<p>Yes, the Board of Directors approved the changes on October 21, 2019 at a face to face
  meeting in Ludwigsburg, Germany.</p>
<h4>Q. Do these changes affect my membership benefits? </h4>
<p>No, these changes do not impact or lesson membership rights or have any impact on our
  classes of membership.</p>
<h4>Q. Are my membership fees changing as a result of these changes to the Bylaws? </h4>
<p>No, these changes do not impact membership fees.</p>
<h4>Q. Who is eligible to vote? </h4>
<p>All Committer Members and Company Representatives are required to vote.</p>
<h4>Q. Do all Committers get to vote? </h4>
<p>Committer Members are Committers who a) work for a Member Company or b) individuals who
  have completed the Committer Membership Agreement. </p>
<h4>Q.  How do I vote?</h4>
<p>Ballots will be sent electronically by email communication shortly and will contain
  instructions on how to submit your ballot.   A simple +1 / 0 / -1 is all that is required,
  and the changes are voted on as a whole package. A prompt response would be greatly appreciated.</p>
<h4>Q. Why should I bother to vote as this doesn&rsquo;t affect me or my organization?</h4>
<p>These changes cannot be adopted without the approval of our members, and in order to do
  that we require a quorum of members to vote.  In short, without your vote, we cannot
  modernize the Bylaws and make positive change for our members.</p>
<h4>Q. How does the voting process work?</h4>
<p>Each Company Representative vote counts as one vote. Each group of committers for a
  Member Company also count as a single vote based on a collapsible single vote. Each vote by an
  individual Committer who has completed the Committer Membership Agreement and is not working for a
  Member Company also counts as one vote. The voting process will close end of business December 12,
  2019 @ 4pm ET. A super-majority of affirmative votes is required for the changes to become
  effective. The explicit rules for voting, quorum, and eligibility are detailed in Section 6 of the
  Eclipse Bylaws<a href="#1">[1]</a>. </p>

<h4>Q. I do not see any of the benefits of membership noted in the Membership Agreement or
  Bylaws; how do I find out about those? </h4>
<p>Members all have certain rights and obligations as noted in the Bylaws and Membership
  Agreement. However, many of the benefits members receive are from programs run by the Eclipse
  Foundation - thanks to the support of its membership. You can find out about these benefits on the
  membership benefits page<a href="#2">[2]</a> and some of the special programs being run on the special programs
  page. New benefits and programs are always being added and you can find out about those from your
  Member Newsletters, Members Meetings, and other information feeds.</p>

<h4>Q. Who do I contact for more information? </h4>
<p>For email questions, please contact license@eclipse.org. If you would like to speak to
  someone directly, please contact Paul White, VP of Member Services, via 1-613-224-9461 ext. 239.</p>
<hr>
<p id="1">
  [1] <a href="/org/documents/eclipse_foundation-bylaws.pdf">/org/documents/eclipse_foundation-bylaws.pdf</a>
</p>
<p id="2">
  [2] <a href="/membership/">/membership/</a>
</p>