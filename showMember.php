<?php
/**
 * Copyright (c) 2014, 2018 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation) - initial API and implementation
 *    Donald Smith (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/classes/membership/membership.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/breadcrumbs.class.php");
require_once("../projects/classes/Project.class.php");

$App = new App();
$Theme = $App->getThemeClass();
$Membership = New Membership();
$Breadcrumb = new Breadcrumb();

// Begin: page-specific settings.  Change these.
$pageTitle  = "Eclipse Membership";
$Theme->setPageKeywords("Eclipse Membership profile page");

$Theme->setPageTitle($pageTitle);

// Place your html content in a file called content/en_pagename.php
ob_start();
include("content/en_" . $App->getScriptName());
$html = ob_get_clean();
$Theme->setHtml($html);

// remove last crumb since it represents this _projectCommon page.
$Breadcrumb->removeCrumb($Breadcrumb->getCrumbCount() -1);
$Breadcrumb->addCrumb("Eclipse Membership", "/membership/exploreMembership.php", "_self");

// Generate the web page
$Theme->setBreadcrumb($Breadcrumb);
$Theme->generatePage();