<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("../_projectCommon.php");    # All on the same line to unclutter the user's desktop'
  
$pageTitle 		= "VOTE NOW";
$pageAuthor = "Donald Smith";
$pageKeywords = "membership, vote2008";

ob_start();
?>
<div id="maincontent">
<div id="midcolumn">

<h1><?=$pageTitle?></h1>

<p>
If you are a Committer Member or a Member Company Representative you should have recieved an email with a link to VOTE!
If you did not receive your official voting
instructions yet, please contact <a href="mailto:membership@eclipse.org">membership at eclipse.org</a>.
</p>


</div><!-- midcolumn -->

</div><!-- maincontent -->
<?php
	# Paste your HTML content between the EOHTML markers!	
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>