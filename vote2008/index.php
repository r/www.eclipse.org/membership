<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("../_projectCommon.php");    # All on the same line to unclutter the user's desktop'

$pageTitle 		= "Eclipse Membership Changes 2008";
$pageAuthor = "Donald Smith";
$pageKeywords = "membership, vote2008";

ob_start();
?>
<div id="maincontent">
<div id="midcolumn">

<h1><?=$pageTitle?></h1>

<p>VOTING CONCLUDED ON JULY 23, 2008.  The Membership at large has overwhelmingly approved the new membership
agreement and bylaws and they have become effective on July 24, 2008.  The Eclipse Foundation would
like to thank the Membership for casting their ballots in a timely manner.  Here are links to the new documents:
<ul>
<li><a href="http://www.eclipse.org/org/documents/Eclipse%20BYLAWS%202008_07_24%20Final.pdf">New Bylaws.</a></li>
<li><a href="http://www.eclipse.org/org/documents/Eclipse%20MEMBERSHIP%20AGMT%202008_07_24%20Final.pdf ">New Membership Agreement.</a></li>
<ul>
</p>
<p>
<b>Final Results:</b><br><br/>
(95.8%) - 157 YES Votes - YES, I agree with the unanimous recommendation of the Eclipse Management Organization and Board of Directors to
update the Membership Agreement and Bylaws.<br>
(4.2%) - 7 NO Votes - NO, I do not support the proposed changes to the Membership Agreement and Bylaws<br>

<br>Quorum rules required 152 votes for the motion to pass - this was achieved.
</p>

<h1>The rest of this page remains for informational/historical purposes.</h1>


<p>
The current <a href="http://www.eclipse.org/org/documents/eclipse_foundation-bylaws.pdf">Bylaws of the Eclipse Foundation </a>
and
<a href="http://www.eclipse.org/org/documents/Eclipse%20MEMBERSHIP%20AGMT%202008_04_16%20Final.pdf">Eclipse Foundation Membership Agreement</a>
have served our community well for almost five years. To prepare the Eclipse Ecosystem for the next five years, a number of changes have been proposed
to the Bylaws and Membership Agreement. These changes require super-majority (two-thirds plus one) approval of the Membership-at-Large to become effective.
</p>

<p>
On June 23, 2008 we initiated an electronic vote of the Eclipse Foundation Membership-at-Large to approve the proposed changes. All Member Company
Representatives and Committers should vote on the proposed changes. If you are a Member Company Represenative or Committer Member and did not
receive your official voting
instructions on June 23, 2008 please contact <a href="mailto:membership@eclipse.org">membership at eclipse.org</a>.
</p>

<h2>Benefits of approving the proposed changes</h2>
<p>
<ul>
<li>Allows Eclipse Foundation to continue growing services and support for members and committers
	<ul>
	<li>More members = more and better services</li>
	</ul>
	</li>
<li>More opportunities for members and projects
	<ul>
	<li>Connections into new markets grows overall ecosystem</li>
	<li>More enterprise use of projects makes them stronger, creates more opportunities for committers</li>
	</ul></li>
<li>Protects health of ecosystem
	<ul>
	<li>More connections to more markets insulates ecosystem from some market conditions</li>
	</ul></li>
</ul>
</p>

<h2>Executive Summary of proposed changes</h2>
<p>
<ul>
<li>Create a new membership class - "Enterprise Member"</li>
<li>Update "Add-In Provider" membership with a more appropriate name - "Solutions Member"</li>
<li>Fix errors and inconsistencies in the current Bylaws</li>
</ul>
</p>

<p>
Here is a summary of the proposed Membership Model:
</p>
<img align="center" width="400px" src="sss.gif">

<h2>Details of proposed changes</h2>
<p>
You have THREE ways to learn more about the membership changes.
<ol>
<li>A PDF slide-format overview with background and details of the
changes can be found on the <a href="/membership/vote2008/v080624%20-%20PROPOSED%20MEMBERSHIP.pdf" target="Membership_Changes">Membership Changes page</a>. </li>
<li>A simple factual summary of the changes:
<ul>
<li>A new membership class called "Enterprise Members" will be created with annual dues of $125,000 USD.</li>
<li>The "Add-In Provider" membership class will be renamed to "Solutions Member."</li>
<li>Enterprise and Solutions Members will be referred collectively as "Sustaining Members."</li>
<li>Sustaining Members will vote for board representation following the same formulae used since the Foundation's inception for
Add-In Provider board representation.</li>
<li>A chronological error in the current Bylaws referring to a deadline of "Action Without Meeting" is corrected.</li>
<li>"Quorum" requirements for future changes to the Membership Agreement and Bylaws have been made consistent
with both methods of doing so (with meeting and without meeting) to be fifty percent plus one. Note that
the high bar of super-majority affirmative votes is maintained. </li>
<li>A logical error referring to a "simple majority" method of collapsing of the ballots
cast by Committers who work for the same Member Company is corrected.</li>
<li>The calculation of election properties (number and term of board representatives) will be based on
a calendar date rather than the moving date of the general meeting at EclipseCon.</li>
</ul>

<li>Legal-ese red-lined versions of the proposed new documents are here:
<ul>
<li><a href="./agreementFINALDRAFT08.pdf">Proposed New Membership Agreement</a> (PDF)</li>
<li><a href="./bylawsFINALDRAFT08.pdf">Proposed New Bylaws</a> (PDF)</li>
</ul>
</li>
</ol>
</p>


<h2>Other recent changes</h2>
<p>The Board of Directors has recently made the following changes to "Exhibit C" of the Membership Agreement.
These changes are made by the Board of Directors and are <b>not</b> part of the voting process:
<ul>
<li>Any organization may become an Associate member with dues of $5,000 per year. Associate membership is still
free for Media, Academia, Government, Not-for-profits and other organization types as defined by the board.</li>
<li>Strategic Developer Minimum Dues have been raised by $25,000.</li>
<li>Add-In Provider Annual Dues for memberships (both new and renewals) on or after July 1, 2008 is tiered based on Corporate Revenue (all values USD):
<ul>
<li>(NEW IN 2010) Annual Corporate Revenue Less Than $1 million, and less than 10 employees and contractors - Fee: 1,500</li>
<li>Annual Corporate Revenue Less Than $10 million - Fee: 5,000</li>
<li>Annual Corporate Revenue Less Than $50 million - Fee: 7,500</li>
<li>Annual Corporate Revenue Less Than $100 million - Fee: 10,000</li>
<li>Annual Corporate Revenue Less Than $250 million - Fee: 15,000</li>
<li>Annual Corporate Revenue Greater Than $250 million - Fee: 20,000</li>
</ul>
</li>
</ul>
</p>

<h2>FAQ</h2>
<p><b>Who do I contact for more information?</b> For email questions, please
contact <a href="mailto:membership@eclipse.org">membership@eclipse.org</a>.
If you would like to speak to someone in North America or APAC, please call Donald Smith at +1 (613) 224-9461 x231.
In Europe and India, please contact Ralph M&uuml;ller
at +49 (177) 4490460.
</p>


<p><b>How does the voting process work?</b>  All Committer Members and Company Representatives are required to vote. Ballots
have been sent electronically by email. Committer
Members are Committers who work for a Member Company or have completed the Committer Membership paperwork. Each Company Representative
vote counts as one vote. Each group of committers for a Member Company also count as a single vote based on a collapsable single vote.
Each vote by a Committer who has completed the Committer Membership paperwork and is not working for a Member Company will also count as one vote.
The voting process will be open for 30 days until close of business July 23, 2008. A super-majority of affirmative votes is required for the changes
to become effective.
</p>


<p><b>Where do I find more details about the new "Enterprise Member" class?</b>  You can find details in the slide-show overview on the
<a href="/go/VOTE2008.PDF" target="Membership_Changes">membership changes</a> page. If you would like
more information, please contact <a href="mailto:membership@eclipse.org">membership at eclipse.org</a>.
</p>

<p><b>I do not see any of the benefits of membership noted in the Membership Agreement or Bylaws; how do I find out about those?</b>  Members
all have certain rights and obligations as noted in the Bylaws and Membership Agreement. However, many of the benefits members receive are
from programs run by the Eclipse Foundation - thanks to the support of its membership. You can find out about these benefits on the
<a href="/membership/become_a_member/benefits.php?member=yes">membership benefits</a> page and some of the
special programs being run on the <a href="/membership/special_programs">special programs</a> page. New benefits and programs are always being added
and you can find out about those from your Member Newsletters, Members Meetings, and other information feeds.
</p>

</div><!-- midcolumn -->

</div><!-- maincontent -->
<?php
	# Paste your HTML content between the EOHTML markers!
	$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>