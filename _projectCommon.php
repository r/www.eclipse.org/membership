<?php
/**
 * Copyright (c) 2018 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation) - initial API and implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */

// Set the theme for your project's web pages.
// See the Committer Tools "How Do I" for list of themes
// https://dev.eclipse.org/committers/
$theme = NULL;
$App->Promotion = TRUE;

// Define your project-wide Nav bars here.
// Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
$Nav->addNavSeparator("Membership", "#");
$Nav->addCustomNav("Explore Members", "/membership/exploreMembership.php", "_self", 1);
$Nav->addCustomNav("Become A Member", "/membership/become_a_member/", "_self", 1);
$Nav->addCustomNav("Members FAQ", "/membership/faq/", "_self", 1);