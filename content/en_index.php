<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation) - initial API and implementation
 *******************************************************************************/
 ?>
<div id="content">
  <div class="margin-bottom-20">
    <!-- Nav tabs -->
    <div class="row content-nav-tab-toggle">
      <div class="visible-xs">
        <div class="vertical-align">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-membership">
            <p class="nav-label"><strong>Eclipse Membership</strong></p>
            <div class="hamburger-wrapper">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </div>
          </button>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="navbar-collapse collapse" id="navbar-membership">
        <ul class="nav nav-tabs solstice-tabs content-nav-tab" role="tablist">
          <li role="presentation" class="active">
            <a href="#tab-benefits" aria-controls="tab-benefits" role="tab" data-toggle="tab" class="background-grey">Benefits and Services</a>
          </li>
          <li role="presentation">
            <a href="#tab-levels" aria-controls="tab-levels" role="tab" data-toggle="tab" class="background-grey">Membership Levels</a>
          </li>
          <li role="presentation">
            <a href="#tab-fees" aria-controls="tab-fees" role="tab" data-toggle="tab" class="background-grey">Membership Fees</a>
          </li>
          <li role="presentation">
            <a href="#tab-membership" aria-controls="tab-membership" role="tab" data-toggle="tab" class="background-grey">Become a Member</a>
          </li>
          <li role="presentation">
            <a href="#tab-resources" aria-controls="tab-resources" role="tab" data-toggle="tab" class="background-grey">Resources</a>
          </li>
          <li role="presentation">
            <a href="#tab-portal" aria-controls="tab-portal" role="tab" data-toggle="tab" class="background-grey">Member Portal</a>
          </li>
        </ul>
      </div>
    </div>

    <!-- Tab panes -->
    <div class="tab-content content-nav-tab-body no-gutters-mobile padding-bottom-40">
      <div role="tabpanel" class="tab-pane text-center active" id="tab-benefits">
        <h2>Why become a Member of the Eclipse Foundation?</h2>
        <p class="margin-bottom-40">Take an active role in supporting sustainable, commercializable open source technologies that benefit all.</p>
        <div class="row section-highlights">
          <div class="col-xs-24 col-sm-12 col-md-8 margin-bottom-20 match-height-item-by-row featured-highlights-item">
            <div class="circle-icon margin-auto">
              <i class="fa fa-file-code-o"></i>
            </div>
            <h3 class="fw-700">Participate in a thriving developer community</h3>
            <p>and provide contributions to <a href="/projects/">Eclipse projects</a> and <a href="/collaborations">Industry Collaborations</a>.</p>
          </div>
          <div class="col-xs-24 col-sm-12 col-md-8 margin-bottom-20 match-height-item-by-row featured-highlights-item">
            <div class="circle-icon margin-auto">
              <i class="fa fa-lock"></i>
            </div>
            <h3 class="fw-700">Protect your strategic investments</h3>
            <p>by signaling your support for advancing the technologies your business relies upon.</p>
          </div>
          <div class="col-xs-24 col-sm-12 col-md-8 margin-bottom-20 match-height-item-by-row featured-highlights-item">
            <div class="circle-icon margin-auto">
              <i class="fa fa-lightbulb-o "></i>
            </div>
            <h3 class="fw-700">Share costs and innovation</h3>
            <p>while focusing resources on rapidly building differentiated features that customers value most.</p>
          </div>
          <div class="col-xs-24 col-sm-12 col-md-8 margin-bottom-20 match-height-item-by-row featured-highlights-item">
            <div class="circle-icon margin-auto">
              <i class="fa fa-balance-scale"></i>
            </div>
            <h3 class="fw-700">Benefit from strong governance</h3>
            <p>solid intellectual property management, and inclusive open source community development.</p>
          </div>
          <div class="col-xs-24 col-sm-12 col-md-8 margin-bottom-20 match-height-item-by-row featured-highlights-item">
            <div class="circle-icon margin-auto">
              <i class="fa fa-handshake-o"></i>
            </div>
            <h3 class="fw-700">Prosper by sharing</h3>
            <p>open source commercialization best practices with industry peers.</p>
          </div>
          <div class="col-xs-24 col-sm-12 col-md-8 margin-bottom-20 match-height-item-by-row featured-highlights-item">
            <div class="circle-icon margin-auto">
              <i class="fa fa-comments"></i>
            </div>
            <h3 class="fw-700">Promote accomplishments</h3>
            <p>and good corporate citizenship through contributions to open source projects.</p>
          </div>
        </div>
        <hr class="orange margin-bottom-40 margin-top-40"/>

        <h2>Key Services of the Eclipse Foundation</h2>
        <p>Members can take advantage of the following key services provided by the Eclipse Foundation.</p>
        <div class="row margin-top-30 margin-bottom-30">
          <div class="col-xs-24 col-sm-12 margin-bottom-20">
            <div class="bordered-box bordered-box-light match-height-item-by-row">
              <div class="box-header">
                <div class="background-secondary padding-10 fw-700">Development Process</div>
              </div>
              <div class="bordered-box-body padding-30">
                <p>The Eclipse community has a well-earned reputation for providing high-quality software, reliably and predictably. The Eclipse Foundation provides services and support to help projects consistently achieve their goals. Our staff assists new projects in implementing the Eclipse development process, a set of procedures and rules that form our best practices.</p>
              </div>
            </div>
          </div>

          <div class="col-xs-24 col-sm-12 margin-bottom-20">
            <div class="bordered-box bordered-box-light match-height-item-by-row">
              <div class="box-header">
                <div class="background-secondary padding-10 fw-700">IP Management</div>
              </div>
              <div class="bordered-box-body padding-30">
                <p>
                We focus on enabling the use of open source technology in commercial software products and services. All Eclipse projects are licensed under an OSI approved license including the <a href="https://www.eclipse.org/legal/epl-2.0/">Eclipse Public License</a> (EPL). In addition, the Eclipse Foundation due diligence process helps to validate the pedigree of the intellectual property contained in Eclipse projects.
                </p>
              </div>
            </div>
          </div>

          <div class="col-xs-24 col-sm-12 margin-bottom-20">
            <div class="bordered-box bordered-box-light match-height-item-by-row">
              <div class="box-header">
                <div class="background-secondary padding-10 fw-700">Ecosystem Development and Marketing</div>
              </div>
              <div class="bordered-box-body padding-30">
                <p>The Eclipse Foundation&rsquo;s vendor-neutral marketing programs promote Eclipse community growth and engagement, drive awareness and discoverability of projects and collaborations and increase the commercial adoption of Eclipse technologies. The Foundation coordinates and implements a variety of activities, including content creation, press and analyst relations, community conferences and events, advertising, social media management, and other programs to promote the entire Eclipse community and increase engagement.</p>
              </div>
            </div>
          </div>

          <div class="col-xs-24 col-sm-12 margin-bottom-20">
            <div class="bordered-box bordered-box-light match-height-item-by-row">
              <div class="box-header">
                <div class="background-secondary padding-10 fw-700">IT Infrastructure</div>
              </div>
              <div class="bordered-box-body padding-30">
                <p>
                The Eclipse Foundation provides vendor-neutral, reliable and scalable services for Eclipse technology developers and users. IT infrastructure services delivered to the Eclipse community include source code repositories, build infrastructure, development-oriented mailing lists and newsgroups, a <a href="https://www.eclipse.org/downloads/">downloads</a> site, and project and working group websites.</p>
              </div>
            </div>
          </div>
        </div>
        <!-- Nav -->
        <div class="margin-top-40 vertical-align text-left">
          <a class="col-xs-22 col-sm-12 col-sm-offset-6 col-md-offset-8 col-md-8 text-center btn btn-primary" href="https://accounts.eclipse.org/contact/membership">Contact Us About Membership</a>
          <a class="hidden-xs col-sm-6 col-md-8 text-right alt-tab-toggle" href="#tab-levels" >Membership levels &gt;</a>
        </div>
      </div>
      <div role="tabpanel" class="tab-pane" id="tab-levels">
        <div class="text-center margin-bottom-30">
          <h2>Membership at the Eclipse Foundation</h2>
          <p>The Foundation offers four levels of membership: <a href="#strategic">Strategic</a>, <a href="#contributing">Contributing</a>, <a href="#associate">Associate</a>, and <a href="#committer">Committer</a>.</p>
        </div>
        <h3 class="brand-primary h4 fw-700" id="strategic">Strategic Members</h3>
        <p>Strategic Members play an integral role in the Eclipse Foundation ecosystem. Strategic members are organizations that view participation in Eclipse projects as strategic to their business and are investing significant developer and other resources to further drive Eclipse Foundation technology. Typically, Strategic Members lead one or more top-level projects. Strategic membership benefits include:</p>
        <ul>
          <li>A seat on Board of Directors of Eclipse Foundation</li>
          <li>A seat on Foundation Architecture Council</li>
          <li>The opportunity to attend and vote at all meetings of the General Assembly</li>
          <li>Access to a number of Strategic Members-Only Programs such as banner and logo promotion on <a href="https://www.eclipse.org/">eclipse.org</a> and <a href="https://www.eclipse.org/downloads/">eclipse.org/downloads</a></li>
          <li>Ad hoc Intellectual Property analysis and reporting</li>
          <li>Help to launch open source initiative(s)</li>
          <li>Direct influence through board voting rights on key aspects of the Eclipse ecosystem, including licensing, governing policy development, and amendments to Membership Agreement and bylaws</li>
          <li>Discounts towards sponsorship and attendance of Eclipse Foundation events</li>
          <li>Access to Eclipse Foundation marketing and <a href="https://outreach.eclipse.foundation/advertise">advertising programs</a></li>
          <li>Opportunity to lead in the establishment of Eclipse Working Groups</li>
          <li>Opportunity to join and participate on all Eclipse Foundation public mailing lists</li>
          <li>Opportunity to show support for the Eclipse Foundation by displaying the Eclipse Foundation member logo</li>
        </ul>
        <p><a href="/membership/exploreMembership.php#tab-strategic">View Existing Strategic Members</a></p>
        <hr class="orange margin-bottom-40 margin-top-40">

        <h3 class="brand-primary h4 fw-700" id="contributing">Contributing Members (formerly referred to as Solutions Members)</h3>
        <p>Contributing Members are organizations that view Eclipse Foundation technologies as important to their corporate and product strategy and offer products and services based on, or with, Eclipse Foundation technologies. These organizations want to participate in the development of the broad Eclipse Foundation ecosystem. Most members join initially as Contributing Members. Contributing membership benefits include:</p>
        <ul>
          <li>Contributing Members are represented on the Board of Directors, and thus have influence over decisions relating to licensing, governing policy development, and amendments to Membership Agreement and bylaws</li>
          <li>Opportunity to stand for election as the representative of all Contributing Members on the Board of Directors. Elections are held annually in the first quarter</li>
          <li>The opportunity to attend and vote at all meetings of the General Assembly</li>
          <li>Discounts towards sponsorship and attendance of Eclipse Foundation run events</li>
          <li>Access to Eclipse Foundation marketing and <a href="https://outreach.eclipse.foundation/advertise">advertising programs</a></li>
          <li>Opportunity to participate in Eclipse Working Groups, including as a Strategic Member of those working groups</li>
          <li>Opportunity to join and participate on all Eclipse Foundation public mailing lists</li>
          <li>Opportunity to show support for the Eclipse Foundation by displaying the Eclipse Foundation Member logo</li>
        </ul>
        <p><a href="/membership/exploreMembership.php#tab-contributing">View Existing Contributing Members</a></p>
        <hr class="orange margin-bottom-40 margin-top-40">

        <h3 class="brand-primary h4 fw-700" id="associate">Associate Members</h3>
        <p>Associate Members are organizations that participate in and want to show support for the Eclipse Foundation ecosystem. Many research and educational institutions choose to join as an Associate Member. Associate membership benefits include:</p>
        <ul>
          <li>Opportunity to join and participate on all Eclipse public mailing lists, and to attend Members meetings</li>
          <li>Ability to join select Eclipse Working Groups as a Guest Member</li>
          <li>The opportunity to attend all meetings of the General Assembly</li>
          <li>Opportunity to show support for the Eclipse Foundation by displaying the Eclipse Foundation Member logo</li>
        </ul>
        <p><a href="/membership/exploreMembership.php#tab-associate">View Existing Associate Members</a></p>
        <hr class="orange margin-bottom-40 margin-top-40">

        <h3 class="brand-primary h4 fw-700" id="committer">Committer Members</h3>
        <p>The Eclipse Foundation’s governance model provides individual committers with the ability to become full members and includes representation on the Eclipse Board of Directors. Committer Members are individuals that are the core developers of the Eclipse projects and can commit changes to project source code.</p>
        <p>Committer membership benefits include:</p>
        <ul>
          <li>Committer Members are represented on the Board of Directors, and thus have influence over decisions relating to licensing, governing policy development, and amendments to Membership Agreement and bylaws</li>
          <li>Opportunity to stand for election as the representative of all Committer Members on the Board of Directors. Elections are held annually in the first quarter</li>
          <li>The opportunity to attend all meetings of the General Assembly</li>
        </ul>
        <p>Learn more about <a href="/membership/become_a_member/committer.php">Committer membership</a>.</p>
        <!-- Nav -->
        <div class="margin-top-40 vertical-align">
          <a class="hidden-xs col-sm-6 col-md-8 text-left alt-tab-toggle" href="#tab-benefits" >&lt; Benefits and Services</a>
          <a class="col-xs-22 col-sm-12 col-md-8 text-center btn btn-primary" href="https://accounts.eclipse.org/contact/membership">Contact Us About Membership</a>
          <a class="hidden-xs col-sm-6 col-md-8 text-right alt-tab-toggle" href="#tab-fees" >Membership Fees &gt;</a>
        </div>
      </div>
      <div role="tabpanel" class="tab-pane text-center" id="tab-fees">
        <h2>Membership Fees</h2>
        <p>The Membership Annual Fee Comparison Chart based on membership levels is provided below for illustration purposes only. Please see the Eclipse Foundation Membership Agreement for full details. Note that Eclipse Working Group Membership fees are separate and in addition to general Eclipse Foundation Membership fees.</p>
        <div class="vertical-align">
          <table class="table table-stripped" cellspacing="0">
            <thead>
                <tr>
                  <th width="40%" rowspan=2>Annual Corporate Revenue</td>
                  <th class="text-center" width="60%" colspan="3">Annual Eclipse Foundation Membership Fees*</th>
                </tr>
                <tr>
                  <th class="text-center" width="20%">Strategic</th>
                  <th class="text-center" width="20%">Contributing</th>
                  <th class="text-center" width="20%">Associate</th>
                </tr>
            </thead>
            <tbody>
              <tr>
                <td class="text-left">&gt; €1&nbsp;billion</td>
                <td>€300 000</td>
                <td>€25 000</td>
                <td>€25 000</td>
              </tr>
              <tr>
                <td class="text-left">€100&nbsp;million - €1&nbsp;billion</td>
                <td>€180 000</td>
                <td>€17 500</td>
                <td>€17 500</td>
              </tr>
              <tr>
                <td class="text-left">€50&nbsp;million - €100&nbsp;million</td>
                <td>€125 000</td>
                <td>€12 500</td>
                <td>€12 500</td>
              </tr>
              <tr>
                <td class="text-left">€10&nbsp;million - €50&nbsp;million</td>
                <td>€60 000</td>
                <td>€9 000</td>
                <td>€9 000</td>
              </tr>
              <tr>
                <td class="text-left">&lt; €10&nbsp;million</td>
                <td>€30 000</td>
                <td>€6 000</td>
                <td>€6 000</td>
              </tr>
              <tr>
                <td class="text-left">&lt; €1&nbsp;million &lt; 10 employees</td>
                <td>€30 000</td>
                <td>€1 500</td>
                <td>€1 500</td>
              </tr>
              <tr>
                <td class="text-left">Govt, Govt agencies, Research Organizations, NGOs, etc.</td>
                <td>€30 000</td>
                <td>€6 000</td>
                <td>€0</td>
              </tr>
              <tr>
                <td class="text-left">Academic, Publishing Organizations, User Groups, etc.</td>
                <td>€30 000</td>
                <td>€1 000</td>
                <td>€0</td>
              </tr>
            </tbody>
          </table>
        </div>
        <p class="text-center">*There is no cost to being a Committer member of the Foundation</p>
        <!-- Nav -->
        <div class="margin-top-40 vertical-align">
          <a class="hidden-xs col-sm-6 col-md-8 text-left alt-tab-toggle" href="#tab-levels" >&lt; Membership Levels</a>
          <a class="col-xs-22 col-sm-12 col-md-8 text-center btn btn-primary" href="https://accounts.eclipse.org/contact/membership">Contact Us About Membership</a>
          <a class="hidden-xs col-sm-6 col-md-8 text-right alt-tab-toggle" href="#tab-membership">How to become a member &gt;</a>
        </div>
      </div>
      <div role="tabpanel" class="tab-pane" id="tab-membership">
        <div class="text-center">
          <h2>How to Become a Member</h2>
          <p>To become a member, follow these steps below:</p>
        </div>
        <p>Once you have chosen the <a class="alt-tab-toggle" href="#tab-levels">level of membership</a> (e.g., Strategic, Contributing, Associate):</p>
        <ol>
          <li>Complete the <a href="https://membership.eclipse.org/application#sign-in">Membership Application Form</a>.</li>
          <li>Once you have completed the Membership Application Form, the membership team will send you the agreements to sign electronically via HelloSign.</li>
        </ol>

        <p>If you are unable to complete this form online, you can use the <a href="#" data-toggle="modal" data-target="#membership-info-modal">PDF form and agreements</a>.</p>
        <div class="modal fade" id="membership-info-modal" tabindex="-1" role="dialog" aria-labelledby="membershipInfoModal">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h1 class="modal-title">PDF form and agreements</h1>
              </div>
              <div class="modal-body">
                <ol class="padding-left-20">
                  <li>Complete and sign the <a href="/membership/documents/membership-application-form.pdf">membership application form pdf</a></li>
                  <li>Complete and sign the <a href="/org/documents/eclipse_membership_agreement.pdf">membership agreement</a></li>
                  <li>Complete and sign the <a href="/legal/committer_process/EclipseMemberCommitterAgreement.pdf">member committer and contributor agreement</a> (optional, but recommended)</li>
                  <li><a href="mailto:membership@eclipse.org">Email us</a> the signed documents, including the membership application form, the membership agreement, and optionally, the member committer and contributor agreement.</li>
                </ol>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>      
              </div>
            </div>
          </div>
        </div>

        <p>Note: Membership in an Eclipse Working Group requires the execution of the respective Working Group’s Participation Agreement. For more information, review the list of the current <a href="/org/workinggroups/explore.php">Eclipse Working Groups</a> and their respective Participation Agreement.</p>

        <p>If you are a Committer who is not already a Member by virtue of your employer being a Member, you may join as an individual <a href="/membership/become_a_member/committer.php">Committer Member</a>.</p>
        <p>Review our <a href="/org/documents/">governance documents</a> for the Eclipse Foundation Bylaws, Membership Agreement, Affliates, Membership Guidelines,
        IP Policy and more.</p>

        <h2>Eclipse Foundation AISBL</h2>
        <p>In January 2021, the Eclipse Foundation created a new Belgian-based organization, Eclipse Foundation AISBL. All new members are strongly encouraged to join this Eclipse Foundation AISBL organization and do so using the above application and agreements.</p>
        <p>Prior to January 2021, Eclipse Foundation membership was based on Eclipse Foundation, Inc., a Delaware-based organization, and this organization still exists. The membership benefits for both organizations are identical, and members are expected to join only one. Any new member of Eclipse Foundation who believes they have reason to join the Delaware-based organization may contact <a href="mailto:membership@eclipse.org">membership@eclipse.org</a> for the appropriate versions of the Membership Agreement and Membership Application.</p>
        <hr class="orange margin-bottom-40 margin-top-40">

        <div class="text-center">
          <h3>Eclipse Corporate Sponsors</h3>
          <p>The Eclipse Foundation relies on the support of our members and contributions from the user community to service and grow the Eclipse ecosystem. In addition to membership, corporate users of Eclipse projects can support the community by joining the <a href="https://www.eclipse.org/corporate_sponsors/" >Corporate Sponsorship Program</a>.</p>
        </div>
        <!-- Nav -->
        <div class="margin-top-40 vertical-align">
          <a class="hidden-xs col-sm-6 col-md-8 text-left alt-tab-toggle" href="#tab-fees" >&lt; Membership Fees</a>
          <a class="col-xs-22 col-sm-12 col-md-8 text-center btn btn-primary" href="https://accounts.eclipse.org/contact/membership">Contact Us About Membership</a>
          <a class="hidden-xs col-sm-6 col-md-8 text-right alt-tab-toggle" href="#tab-resources" >Resources &gt;</a>
        </div>
      </div>
      <div role="tabpanel" class="tab-pane text-center" id="tab-resources">
        <h2>Resources</h2>
        <div class="row section-highlights margin-top-30 margin-bottom-20">
          <div class="col-xs-24 col-sm-12 margin-bottom-20 featured-highlights-item">
            <div class="circle-icon margin-auto">
              <i class="fa fa-file-text-o"></i>
            </div>
            <div class="match-height-item-by-row margin-bottom-20">
              <h3 class="h4 fw-700"><a href="/membership/documents/membership-prospectus.pdf">Membership Prospectus</a></h3>
              <p>Learn about Eclipse Foundation membership, and how you can become a member.</p>
            </div>
            <p><a class="btn btn-primary" href="/membership/documents/membership-prospectus.pdf">Download</a></p>
          </div>
          <div class="col-xs-24 col-sm-12 margin-bottom-20 featured-highlights-item">
            <div class="circle-icon margin-auto">
              <i class="fa fa-file-powerpoint-o"></i>
            </div>
            <div class="match-height-item-by-row margin-bottom-20">
              <h3 class="h4 fw-700"><a href="/membership/documents/eclipse-foundation-overview.pdf">Overview Presentation</a></h3>
              <p>A presentation of the Eclipse Foundation and Eclipse community for new and prospective members.</p>
            </div>
            <p><a class="btn btn-primary" href="/membership/documents/eclipse-foundation-overview.pdf">Download</a></p>
          </div>
        </div>
        <h3>Keeping Members Connected</h3>
        <p>Serving our community is the number one priority of the Eclipse Foundation.To keep our community connected, we send out a monthly newsletter to member companies and member committers. Check out some <a href="/community/newsletter/">past issues of our member newsletter</a>.

        <!-- Nav -->
        <div class="margin-top-40 vertical-align">
          <a class="hidden-xs col-sm-6 col-md-8 text-left alt-tab-toggle" href="#tab-membership">&lt; How to become a member</a>
          <a class="col-xs-22 col-sm-12 col-md-8 text-center btn btn-primary" href="https://accounts.eclipse.org/contact/membership">Contact Us About Membership</a>
          <a class="hidden-xs col-sm-6 col-md-8 text-right alt-tab-toggle" href="#tab-portal">Member Portal &gt;</a>
        </div>
      </div>

      <div role="tabpanel" class="tab-pane text-center" id="tab-portal">
        <h2>Member Portal</h2>
        <div class="row section-highlights margin-top-30 margin-bottom-20">
          <p>
            The Eclipse Foundation member portal provides Member organizations with insights into their participation in the Eclipse community. Find out about industry collaborations and projects your organization is involved in and access resources curated for Members. 
          </p>
          <p>
            Additionally, this portal allows members to update key data related to their membership, including their logo, corporate description, and key contacts.
          </p>
        </div>

        <!-- Nav -->
        <div class="margin-top-40 vertical-align">
          <a class="hidden-xs col-sm-6 col-md-8 col-sm-pull-3 col-md-pull-4 text-left alt-tab-toggle" href="#tab-resources">&lt; Resources</a>
          <a class="col-xs-22 col-sm-12 col-sm-pull-3 col-md-pull-4 col-md-8 btn btn-primary" href="https://membership.eclipse.org/portal">Go to the Member Portal</a>
        </div>
      </div>
    </div>
  </div>
</div>