<?php

/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation) - initial API and implementation
 *******************************************************************************/
?>

<div class="eclipsefdn-members margin-bottom-20">
  <div class="eclipsefdn-members-list">
    <div class="text-center margin-top-30">
      <i class="fa fa-spinner fa-pulse fa-2x fa-fw margin-bottom-20"></i>
      <span class="sr-only">Loading...</span>
    </div>
  </div>
  <a class="col-xs-18 col-sm-12 col-sm-offset-6 col-md-offset-8 col-md-8 text-center btn btn-primary margin-bottom-10" href="#">Become a Member</a>
</div>