<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - initial API and implementation
 *******************************************************************************/
?>
<div class="row">
  <div class="col-sm-12">
    <p>Please submit your email address to verify that you are a contact for <strong><?php print $this->getMemberName(); ?></strong></p>
    <p>If you are listed as a contact you will receive a temporary access token.</p>
    <div class="well">
      <form method="POST" class="form-inline">
        <div class="form-group">
          <label for="exampleInputEmail1">Email address</label>
          <input type="email" class="form-control" name="token_request_email" placeholder="Email">
        </div>
        <input name="state" type="hidden" value="token-request">
        <input type="hidden" value="<?php print $this->id; ?>">
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
  </div>
</div>
