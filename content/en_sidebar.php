<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation) - initial API and implementation
 *******************************************************************************/
 ?>
<div class="col-md-9 col-lg-7 col-md-offset-1 margin-top-20 margin-bottom-20">
  <div class="icon-sidebar-menu row">
    <?php if (isset($show_membership_contact) && $show_membership_contact == TRUE) :?>
      <div class="padding-bottom-20 clearfix margin-bottom-15">
        <div class="col-xs-8 col-md-9 hidden-xs hidden-sm">
          <a class="circle-icon" href="https://accounts.eclipse.org/contact/membership">
            <i class="fa fa-envelope-o orange"></i>
          </a>
        </div>
        <div class="col-sm-24 col-md-15">
          <h3><a href="https://accounts.eclipse.org/contact/membership">Contact us about membership</a></h3>
          <p>Contact us about your organization joining the Eclipse Foundation</p>
        </div>
      </div>
    <?php endif;?>
    <?php if (!isset($hide_membership) || $hide_membership == FALSE) :?>
      <div class="padding-bottom-20 clearfix">
        <div class="col-md-9 hidden-xs hidden-sm">
          <a class="circle-icon" href="/membership/exploreMembership.php">
            <i class="fa fa-search orange"></i>
          </a>
        </div>
        <div class="col-sm-24 col-md-15">
          <h3><a href="/membership/exploreMembership.php">Explore our Members</a></h3>
          <p>Learn more about the products and services provided by the members of Eclipse</p>
        </div>
      </div>
    <?php endif;?>
    <?php if (!isset($hide_become) || $hide_become == FALSE) :?>
      <div class="padding-bottom-20 clearfix margin-bottom-15">
        <div class="col-xs-8 col-md-9 hidden-xs hidden-sm">
          <a class="circle-icon" href="/membership/become_a_member/">
            <i class="fa fa-user orange"></i>
          </a>
        </div>
        <div class="col-sm-24 col-md-15">
          <h3><a href="/membership/become_a_member/">Become a member</a></h3>
          <p>Join the Eclipse Foundation and influence the future</p>
        </div>
      </div>
    <?php endif;?>
    <?php if (isset($show_corporate_sponsors_block) && $show_corporate_sponsors_block == TRUE) :?>
      <div class="padding-bottom-20 clearfix margin-bottom-15">
        <div class="col-xs-8 col-md-9 hidden-xs hidden-sm">
          <a class="circle-icon" href="/membership/become_a_member/">
            <i class="fa fa-handshake-o orange"></i>
          </a>
        </div>
        <div class="col-sm-24 col-md-15">
          <h3><a href="/corporate_sponsors">Become a corporate sponsor</a></h3>
          <p>We encourage corporate users of Eclipse technologies to support the community by becoming a member and/or joining the Corporate Sponsorship Program.</p>
        </div>
      </div>
    <?php endif;?>
  </div>
</div>

<?php if (isset($show_modal) && $show_modal == TRUE) :?>
  <div class="col-md-10 col-lg-7 col-lg-offset-1 pull-right">
    <div class="sideitem">
      <h6>Helpful tips</h6>
      <ul class="fa-ul">
        <li><i class="fa-li fa fa-chevron-circle-right orange"></i><a href="mailto:membership@eclipse.org">Email us</a> if you have any questions.</li>
        <li><i class="fa-li fa fa-chevron-circle-right orange"></i><a href="#" data-toggle="modal" data-target="#modal-checklist">Check list</a> of sections to be completed on the membership agreement </li>
        <li><i class="fa-li fa fa-chevron-circle-right orange"></i><a href="/membership/become_a_member/benefits.php">Benefits of Membership</a></li>
        <li><i class="fa-li fa fa-chevron-circle-right orange"></i>Eclipse <a href="/org/documents/">Governance Documents</a>, including Bylaws, Affiliates Membership Guidelines, IP Policy, Eclipse Public License (EPL), and others</li>
        <li><i class="fa-li fa fa-chevron-circle-right orange"></i>Your organization can also support the Eclipse Foundation and community through the <a href="/corporate_sponsors/">Corporate Sponsor program</a>.</li>
      </ul>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade solstice-modal" id="modal-checklist" tabindex="-1" role="dialog" aria-labelledby="modal-checklistLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-content-container">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modal-checklistLabel">
             Here are some frequently overlooked sections in the Membership Agreement:</h4>
          </div>
          <div class="modal-body">
            <ol class="circles-list">
              <li style="padding-top:5px;">Place the effective date on <strong>page 1</strong>.</li>
              <li style="padding-top:5px;">Place your organization name in the blank on <strong>page 1</strong>.</li>
              <li>Fill in the Member and Member Notice information on <strong>page 7</strong> (bottom half of page).</li>
              <li>Sign (on the line "By:" on the bottom half of the page) and date the Agreement on <strong>page 7</strong>.</li>
              <li>Complete the information in <strong>Exhibit C</strong> by checking the box in the column "Check to Indicate Desired Level of Membership". <strong><br/>Please sign and date Exhibit C</strong></li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif;?>