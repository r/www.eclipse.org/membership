<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation) - initial API and implementation
 *******************************************************************************/
?>

<div class="col-md-14 col-lg-17">
  <ul class="nav nav-tabs" role="tablist">
    <li class="active"><a href="#open_tab_edit-member-info" role="tab" data-toggle="tab" id="tab-edit-member-info">Member Information</a></li>
    <li><a href="#open_tab_edit-logos" role="tab" data-toggle="tab" id="tab-edit-logos">Logos</a></li>
    <li><a href="#open_tab_edit-links" role="tab" data-toggle="tab" id="tab-edit-links">Links</a></li>
    <li><a href="#open_tab_view-contacts" role="tab" data-toggle="tab" id="tab-view-contacts">Contacts</a></li>
  </ul>
  <div class="tab-content">
    <div id="open_tab_edit-member-info" class="tab-pane fade in active">
      <form method="POST" class="form-horizontal">
        <div class="form-group">
          <label for="member_short_description" class="col-sm-6 control-label">Short description</label>
          <div class="col-sm-18">
            <textarea name="member_short_description" class="form-control" rows="3" maxlength="250"><?php print $this->getMemberShortDescription();?></textarea>
            <span id="helpBlock_long_description" class="help-block small">Maximum length: 250 characters.</span>
          </div>
        </div>
        <div class="form-group">
          <label for="member_long_description" class="col-sm-6 control-label">Long description</label>
          <div class="col-sm-18">
            <textarea name="member_long_description" class="form-control" rows="6"><?php print $this->getMemberLongDescription();?></textarea>
            <span id="helpBlock_long_description" class="help-block small">Allowed HTML tags: &lt;p&gt;&lt;strong&gt;&lt;em&gt;&lt;b&gt;&lt;i&gt;&lt;br&gt;&lt;ul&gt;&lt;li&gt;. <br>Any other tags will be removed.</span>
          </div>
        </div>

        <div class="form-group">
          <label for="company_url" class="col-sm-6 control-label">Company URL</label>
          <div class="col-sm-18">
            <input type="text" name="member_url" class="form-control" value="<?php print $this->getMemberUrl();?>" />
          </div>
        </div>
        <input name="state" type="hidden" value="edit-info">
        <input name="member_id_for_info" type="hidden" value="<?php print $this->id;?>">
        <hr>
        <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Submit</button>
      </form>
    </div>
    <div id="open_tab_edit-logos" class="tab-pane fade">
      <h2>Logos</h2>
      <div class="row">
        <div class="col-sm-12">
          <h3>Edit small logo <i class="fa fa-pencil" style="color:#ccc;"></i></h3>
          <p>Small Logos are displayed on the Explore Eclipse Membership page</p>
          <div class="solstice-block-white-bg"><?php print $this->getMemberLogo('small');?></div>
          <form method="POST" enctype="multipart/form-data">
            <label for="member_small_logo">Select new image:</label>
            <input type="file" accept="image/*" name="member_small_logo" id="member_small_logo">
            <p class="help-block small">Supported format: png, jpg, gif<br>
            Max width and height: 120px<br>
            If the width of height exeeds 120px, the logo will be resized.</p>
            <input name="MAX_FILE_SIZE" value="102400" type="hidden">
            <input name="member_id_for_small_logo" type="hidden" value="<?php print $this->id;?>">
            <input name="logo-size" value="small" type="hidden">
            <input name="state" value="edit-logo" type="hidden">
            <hr>
            <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Submit</button>
            <input type="reset" value="Cancel" class="btn">
          </form>
        </div>
        <div class="col-sm-12">
          <h3>Edit large Logo <i class="fa fa-pencil" style="color:#ccc;"></i></h3>
          <p>Large logos are displayed on your main membership information
          page. You can submit your logo in any size, but the larger the
          logo the more space it will take up on your membership page.
          We recommend using a logo of between 200 and 250px width for best look at feel.</p>
          <div class="solstice-block-white-bg"><?php print $this->getMemberLogo('large');?></div>
          <form method="POST" enctype="multipart/form-data">
            <label for="large_logo_upload">Select new image:</label>
            <input type="file" accept="image/*" name="member_large_logo" id="member_large_logo">
            <p class="help-block small">Supported format: png, jpg, gif<br>
            Max width and height: 200px<br>
            If the width of height exeeds 200px, the logo will be resized.</p>
            <input name="MAX_FILE_SIZE" value="102400" type="hidden">
            <input name="member_id_for_large_logo" type="hidden" value="<?php print $this->id;?>">
            <input name="logo-size" value="large" type="hidden">
            <hr>
            <input name="state" type="hidden" value="edit-logo">
            <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Submit</button>
            <input type="reset" value="Cancel" class="btn">
          </form>
        </div>
      </div>
    </div>
    <div id="open_tab_edit-links" class="tab-pane fade">
      <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <h2>Links</h2>
        <p>This tab enables you to add links on your membership
            page to products and services that you wish to highlight.
            Simply click "[add]" to add new entries, or "[edit]" to
            edit an existing entry. To remove an entry, simply use the delete button
            for each links you'd like to remove. </p>
            <p>In addition to your explicit links included here, your
            organization's listings on the <a href="http://marketplace.eclipse.org/">Eclipse Marketplace</a> will
            automatically appear on your membership page.  Please note
            that these marketplace listings may be edited from the
            Marketplace only.  If you have listings in Marketplace
            that are not appearing on your membership page, please
            ensure that your organization (company name) on the
            Marketplace listings is the same as it appears on your
            membership page (including suffixes such as Inc, Gmbh, etc).</p>
            <p>Please email <a href="mailto:membership-admin@eclipse.org">membership-admin@eclipse.org</a> if you require assistance.</p>
        <div class="row">
          <div class="col-sm-12">
            <h3>Edit Links</h3>
            <?php foreach ($this->getMemberProduct() as $product): ?>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading<?php print $product['id']; ?>">
                  <h4 class="panel-title">
                    <a class="solstice-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php print $product['id']; ?>" aria-expanded="false" aria-controls="collapse<?php print $product['id']; ?>">
                      <i class="fa fa-chevron-down"></i> <?php print $product['name']; ?>
                    </a>
                    <i class="fa fa-pencil" style="color:#fff; float:right;"></i>
                  </h4>
                </div>
                <div id="collapse<?php print $product['id']; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php print $product['id']; ?>">
                  <div class="panel-body">
                    <div class="product-<?php print $product['id']; ?>">
                      <form method="POST" class="form-horizontal">
                        <div class="form-group">
                          <label for="member_product_name" class="col-sm-6 control-label">Title:</label>
                          <div class="col-sm-18">
                            <input type="text" name="member_product_name" class="form-control name" value="<?php print $product['name'];?>" />
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="member_product_description" class="col-sm-6 control-label">Description:</label>
                          <div class="col-sm-18">
                            <textarea rows="4" name="member_product_description" class="form-control description"><?php print $product['teaser'];?></textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="member_product_url" class="col-sm-6 control-label">Url:</label>
                          <div class="col-sm-18">
                            <input type="url" name="member_product_url" class="form-control url" value="<?php print $product['url'];?>" />
                          </div>
                        </div>
                        <hr>
                        <input name="state" type="hidden" value="edit-link">
                        <input type="hidden" name="member_product_id" value="<?php print $product['id']; ?>">
                        <button class="btn btn-primary float-left" type="submit"><i class="fa fa-check"></i> Submit</button>

                      </form>
                      <form method="POST">
                        <input name="state" type="hidden" value="delete-link">
                        <input type="hidden" name="member_product_id" value="<?php print $product['id']; ?>">
                        <button type="submit" class="btn btn-danger float-left margin-right-5"><i class="fa fa-trash"></i> Delete</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            <?php endforeach; ?>
          </div>
          <div class="col-sm-12">
            <h3>Add a new link <i class="fa fa-plus" style="color:#ccc; float:right;"></i></h3>
            <div class="well well-sm">
            <form method="POST" class="form-horizontal">
              <div class="form-group">
                <label for="new_member_product_name" class="col-sm-6 control-label">Title:</label>
                <div class="col-sm-18">
                  <input type="text" name="new_member_product_name" class="form-control" placeholder="Title" />
                </div>
              </div>
              <div class="form-group">
                <label for="new_member_product_description" class="col-sm-6 control-label">Description:</label>
                <div class="col-sm-18">
                  <textarea rows="4" name="new_member_product_description" class="form-control" placeholder="Description" ></textarea>
                </div>
              </div>
              <div class="form-group">
                <label for="new_member_product_url" class="col-sm-6 control-label">Url:</label>
                <div class="col-sm-18">
                  <input type="url" name="new_member_product_url" class="form-control" placeholder="Url" />
                </div>
              </div>
              <hr>
              <input name="state" type="hidden" value="add-link">
              <input type="hidden" name="new_member_product_organization_id" value="<?php print $this->id; ?>">
              <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Submit</button>
              <input type="reset" value="Cancel" class="btn btn-secondary">
            </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="open_tab_view-contacts" class="tab-pane fade">
      <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <h2>Contacts</h2>
        <p>The people listed on this page can edit your
          Eclipse membership pages.  All changes to the contacts will be submitted
          to the Eclipse Foundation's membership management for review.</p>
        <div class="row">
          <div class="col-sm-12">
            <h3>Current Contacts</h3>
            <?php foreach($this->fetchMemberMaintainers($this->id) as $contact): ?>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading<?php print $contact['PersonID']; ?>">
                  <h4 class="panel-title">
                    <a class="solstice-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php print $contact['PersonID']; ?>" aria-expanded="false" aria-controls="collapse<?php print $contact['PersonID']; ?>">
                      <i class="fa fa-chevron-down"></i> <?php print $contact['FName'] . ' ' . $contact['LName']; ?>
                    </a>
                  </h4>
                </div>
                <div id="collapse<?php print $contact['PersonID']; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php print $contact['PersonID']; ?>">
                  <div class="panel-body">
                  <ul>
                    <li><strong>First name:</strong> <?php print $contact['FName']; ?></li>
                    <li><strong>Last name:</strong> <?php print $contact['LName']; ?></li>
                    <li><strong>Email:</strong> <a href="mailto:<?php print $contact['EMail']; ?>"><?php print $contact['EMail']; ?></a></li>
                    <li><strong>Phone:</strong> <?php print $contact['Phone']; ?></li>
                    <li><strong>Role(s):</strong> <?php print $contact['Type']; ?></li>
                  </ul>
                  </div>
                </div>
              </div>
            <?php endforeach; ?>
            <h3>Delete a contact</h3>
            <div class="well well-sm">
              In order to delete a contact from the list, you must contact
              <a href="mailto:membership-admin@eclipse.org?subject=Delete a Membership contact">membership-admin@eclipse.org</a>.
            </div>
          </div>
          <div class="col-sm-12">
            <h3>Add a new contact <i class="fa fa-plus" style="color:#ccc; float:right;"></i></h3>
            <div class="well well-sm">
              <form method="POST">
                <div class="form-group">
                  <label for="new_maintainer_last_name">First Name</label>
                  <input type="text" class="form-control" name="new_maintainer_first_name" placeholder="First Name">
                </div>
                <div class="form-group">
                  <label for="new_maintainer_last_name">Last Name</label>
                  <input type="text" class="form-control" name="new_maintainer_last_name" placeholder="Last Name">
                </div>
                <div class="form-group">
                  <label for="new_maintainer_email">Email address</label>
                  <input type="email" class="form-control" name="new_maintainer_email" placeholder="Email">
                  <p class="help-block small">Please use a company email address.</p>
                </div>
                <div class="form-group">
                  <label for="new_maintainer_phone">Phone Number</label>
                  <input type="text" class="form-control" name="new_maintainer_phone" placeholder="Phone Number">
                </div>
                <div class="form-group">
                  <label for="new_maintainer_type">Contact Roles:</label>
                  <select multiple class="form-control" name="new_maintainer_type[]">
                    <option selected value="Membership Page Editor">Membership Page Editor</option>
                    <option value="Marketing">Marketing</option>
                    <option value="Company Representative">Company Representative</option>
                    <option value="Delegate">Delegate</option>
                  </select>
                  <a class="small" href="#view-contacts" data-toggle="popover" title="Information about the roles" data-content="<p><strong>Membership Page Editor:</strong> Can update the membership page</p><p><strong>Delegate:</strong> Standin for the Company Representative</p><p><strong>Company Representative:</strong> Main contact for the membership</p><p><strong>Accounting Representative:</strong> Main contact for invoicing</p>">Information about the roles</a>
                </div>
                <input name="state" type="hidden" value="add-contact">
                <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Submit</button>
                <input type="reset" value="Cancel" class="btn btn-secondary">
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
<div class="col-md-10 col-lg-7">
  <div class="solstice-block-white-bg">
    <div class="text-center"><?php print $this->getMemberLogo('small');?></div>
    <hr>
    <ul class="nav">
      <li><a href="https://www.eclipse.org/membership/showMember.php?member_id=<?php print $this->id; ?>">View Member</a></li>
      <li><a href="https://www.eclipse.org/membership/editMember.php?member_id=<?php print $this->id; ?>"><i class="fa fa-chevron-circle-right orange"></i> <strong>Edit Member</strong></a></li>
      <li><a href="https://accounts.eclipse.org/membership/<?php print $this->id; ?>/employees">View Employees</a></li>
    </ul>
  </div>
</div>
