<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation) - initial API and implementation
 *******************************************************************************/
 ?>
<div class="col-md-14 col-lg-16">
  <h1 class="red">INVALID MEMBER ID</h1>
  <p>No member could be found matching that ID.  There are three possible reasons
  why this can happen:</p>
  <ul>
    <li>You reached this page through a bad link (malformed HTML),</li>
    <li>this organization is no longer an active member,</li>
    <li>OR, this organization has not yet setup their membership page.</li>
  </ul>

  <p>Please <a href="mailto:membership@eclipse.org">email us</a> if you believe this
  is an error we can fix or better yet --
  <a href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Community&amp;version=unspecified&amp;component=Website&amp;rep_platform=PC&amp;op_sys=Windows%20XP&amp;priority=P3&amp;bug_severity=normal&amp;bug_status=NEW&amp;bug_file_loc=http%3A%2F%2F&amp;short_desc=Eclipse%20Membership%20Pages%20Suggestion%20or%20Issue&amp;comment=&amp;commentprivacy=0&amp;maketemplate=Remember%20values%20as%20bookmarkable%20template&amp;form_name=enter_bug&amp;cc=membership%40eclipse.org">open a bug</a>.
  </p>
</div>
<?php include_once 'en_sidebar.php';?>
