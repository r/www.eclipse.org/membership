<?php
/**
 * Copyright (c) 2014, 2018, 2023 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Denis Roy (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *    Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
$App = new App();
$Theme = $App->getThemeClass();

// Begin: page-specific settings.  Change these.
$pageTitle = "Explore Our Members";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("Eclipse membership, eclipse members, Become an Eclipse member, Membership FAQ");

$hide_membership = TRUE;
$hide_faqs = TRUE;
$hide_tips = TRUE;
$show_corporate_sponsors_block = TRUE;

ob_start();
?>
<div class="jumbotron featured-jumbotron featured-jumbotron-dark featured-jumbotron-astro margin-bottom-0">
  <div class="container">
    <div class="row">
      <div class="col-md-20 col-md-offset-2 col-sm-18 col-sm-offset-3">
        <h1 class="featured-jumbotron-headline"><?php print $pageTitle; ?></h1>
        <p class="featured-jumbotron-subtitle">
          Meet the Eclipse Foundation members and learn more about the products and services they provide.
        </p>
        <ul class="hidden-xs list-inline margin-top-30">
          <li><a class="btn btn-primary" href="https://accounts.eclipse.org/contact/membership">Become a Member</a></li>
          <li><a class="btn btn-primary" href="/org/workinggroups/explore.php">Industry Collaborations</a></li>
        </ul>
        <ul class="visible-xs list-inline margin-top-30">
          <li class="col-xs-20 col-xs-offset-2"><a class="btn btn-primary btn-wide" href="https://accounts.eclipse.org/contact/membership">Become a Member</a></li>
          <li class="col-xs-20 col-xs-offset-2"><a class="btn btn-primary btn-wide" href="/org/workinggroups/explore.php">Industry Collaborations</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<?php
$extra_header_html = ob_get_clean();
$Theme->setExtraHeaderHtml($extra_header_html);

// Place your html content in a file called content/en_pagename.php
ob_start();
include("content/en_" . $App->getScriptName());
$html = ob_get_clean();
$Theme->setHtml($html);

// remove breadcrumb margin bottom
$Theme->removeAttributes('breadcrumbs', 'breadcrumbs-default-margin');
$Theme->setAttributes('header-wrapper', 'header-default-bg-img');
$Theme->setAttributes('main', 'background-white');
// Generate the web page
$Theme->generatePage();
