<?php
/**
 * Copyright (c) 2014, 2018 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Wayne Beaton (Eclipse Foundation)- initial API and implementation
 *    Christopher Guindon (Eclipse Foundation) - Re-implementation for the new solstice theme
 *
 * SPDX-License-Identifier: EPL-2.0
 */

header('Location: /membership', TRUE, 301);
exit;