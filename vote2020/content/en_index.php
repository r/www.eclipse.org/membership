<?php
/**
 * Copyright (c) 2019 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<h1><?php print $pageTitle;?></h1>

<p>The voting period ended on September 29, 2020 with the modifications to the Eclipse Foundation, Inc.’s Bylaws and Membership Agreement being ratified by a 97% majority. These new Bylaws and Membership Agreement are effective as of October 1, 2020, and are available on our <a href="https://www.eclipse.org/org/documents/">Governance Documents</a> page.</p>

<p>The results of the vote were:</p>
<ul>
  <li>YES: (97%)</li>
  <li>NO: (3%)</li>
</ul>

<p>The Eclipse Foundation, Inc. received approval of its Board of Directors to update the Eclipse Foundation, Inc.&rsquo;s Bylaws and Membership Agreement at its August 19, 2020, meeting.</p>

<ul>
	<li><a href="#proposed_bylaw">Summary of EF-US Bylaws changes</a></li>
	<li><a href="/membership/vote2020/documents/bylaws_proposed.pdf">Revised EF-US Bylaws - clean version</a></li>
	<li><a href="/membership/vote2020/documents/bylaws_proposed_annotated.pdf">Revised EF-US Bylaws annotated</a></li>
	<li><a href="#proposed_membership">Summary of EF-US Membership changes</a></li>
	<li><a href="/membership/vote2020/documents/membership_proposed.pdf">Revised EF-US Membership - clean version</a></li>
	<li><a href="/membership/vote2020/documents/membership_proposed_annotated.pdf">Revised EF-US Membership Agreement annotated</a></li>
</ul>

<h2>Background:</h2>

<p>On May 12, 2020, the Eclipse Foundation <a href="https://newsroom.eclipse.org/news/announcements/open-source-software-leader-eclipse-foundation-announces-transition-europe-part">announced</a> its transition to Europe as part of its continued global expansion.</p>

<p>As part of that transition, all incorporation and related documents have been filed with the Belgian authorities and the Eclipse Foundation awaits the Royal Decree to formally create the new entity, the Eclipse Foundation AISBL.&nbsp; The Decree is expected to be finalized by early-to-mid October.&nbsp; In parallel, the Board of Directors of the Eclipse Foundation, Inc. have approved a series of changes to the Bylaws and Membership Agreement of the existing Delaware, USA-based entity, in order to align and support the European transition ensuring the governance of the US organization aligns with the new Belgian organization.</p>

<p>In addition to Board approval, changes to the Bylaws and Membership Agreement require a vote of the Membership At-Large.&nbsp; This includes all classes of membership except Associate Members and does include Committer Members.&nbsp; The approval of a super-majority (two thirds) of the Membership At-Large is required for these changes to go into effect.</p>

<p>The membership vote will run through September 29, 2020.&nbsp; Ballots will be distributed by email to all Company Representatives and Committer Members on August 31, 2020.&nbsp;</p>

<p>We urge all Members to vote to enable these important changes.</p>

<h2 id="proposed_bylaw">The proposed EF-US Bylaw changes are:</h2>
<ul>
	<li>Realign the definition of the Purposes of the Foundation to reflect all Eclipse technology to better recognize the vast technologies hosted by the Foundation&nbsp;&nbsp;</li>
	<li>Simplify Membership Classes to Strategic, Contributing (formerly Solutions), Associate and Committer</li>
	<li>Add &ldquo;Strategic Foundation&rdquo; Membership class to enable the new Belgian organization to become a member of the Delaware-based organization</li>
	<li>Clarify Member qualifications with respect to Affiliated organizations, notably to clarify that affiliated organizations may participate as a single Member should they choose to</li>
	<li>Change quorum requirements to reduce the quorum for votes of the membership-at-large to 5% for normal decisions and to 33% for major decisions</li>
	<li>Clarify the rights of Board members to have a representative vote at Board meetings on their behalf</li>
	<li>Clarify the nomination process for the Board to nominate members to its Compensation Committee&nbsp;</li>
	<li>Clarify that the removal of a Board member &ldquo;for cause&rdquo; requires a majority vote of the Membership At-Large</li>
</ul>

<h2 id="proposed_membership">The proposed EF-US Membership Agreement changes are:</h2>
<ul>
	<li>Simplify membership classes to Strategic, Contributing (formerly Solutions), Associate and Committer.</li>
	<li>Add Strategic Foundation Membership class</li>
	<li>Clarify use of member name in addition to logo use</li>
	<li>Clarify the requirement of Strategic Members to have two full-time equivalent developers engaged in Eclipse projects</li>
</ul>

<h2>FAQ</h2>

<p><strong>Q. Why is the Eclipse Foundation making these changes now? </strong></p>

<p>In order to accommodate the transition to Europe, both the Bylaws and the Membership Agreement for the existing US organization are being modified.&nbsp; At a high level, the changes being made are to keep the two organizations in alignment with respect to classes of membership, membership fees, etc., and to ensure the governance of the US organization aligns with the new Belgian organization.&nbsp;</p>

<p><strong>Q. Why is a vote required to make this change?</strong></p>

<p>The Foundation&rsquo;s Bylaws explicitly state that any change to the Bylaws and/or Membership Agreement must be approved by the Board of Directors as well a vote must be held of the existing members.</p>

<p><strong>Q. Has the Foundation&rsquo;s Board of Directors approved this change?</strong></p>

<p>Yes, the Board of Directors approved these changes on August 19, 2020.</p>

<p><strong>Q.&nbsp; Why did you make changes to the Membership Classes?</strong></p>

<p>As part of the transition, it was a great opportunity to simplify and clarify our membership classes, and to correspondingly simplify and shorten the membership agreement.</p>

<p><strong>Q. Why was a new membership level of Strategic Foundation added?</strong></p>

<p>A new class of membership called Strategic Foundation member has been introduced into the bylaws of the existing US organization. By doing this, the new Belgian foundation will be an active member of the US organization, and will help participate in its governance by having seats on its Board. We would describe this as an &ldquo;implementation detail&rdquo;, and would be happy to provide more detail should you have questions about it.</p>

<p><strong>Q. Why did we change Solutions membership to Contributing membership? Are there other changes to the classes of membership?</strong></p>

<p>We are changing the name of the Solutions membership level to Contributing membership to better reflect the current, diverse group of organizations that make up our membership base. These organizations participate and contribute to the development of the Eclipse ecosystem in many ways, including, but not limited to, leading and contributing to Eclipse Foundation projects, and offering products and services based on Eclipse technologies.<br />
<br />
The Strategic, Committer, and Associate membership classes all remain. For completeness, we have also eliminated the Strategic Consumer and Enterprise classes of membership.&nbsp; These have not been used for many years, and do not reflect how our members participate.</p>

<p><strong>Q. Are my membership fees changing as a result of these changes to the Bylaws? </strong></p>

<p>The Board of Directors has already approved the fee changes that come into effect October 1, 2020 for both the Beligan-based Eclipse Foundation AISBL and the existing Eclipse Foundation, Inc. The fees are now stated in Euros while retaining the same numeric value, e.g. if you are currently paying $20,000 the dues in the new organization will be 20.000&euro;. However, we do understand this represents a dues increase, and to help mitigate the increase, all member renewal fees between October 1, 2020 and September 30, 2021 will be discounted by 10%. So, to continue this example, if your membership fee in 2020 was $20,000, your fee will be 18.000&euro; in 2021, and 20.000&euro; in 2022 and beyond.&nbsp;</p>

<p>This modest fee increase is the first in almost 15 years - we hope that members appreciate the merit of stating all fees in Euros as we complete this transition.</p>

<p>Note that working group fees are not changing as a result of the move, and will remain in USD.&nbsp; Each working group&rsquo;s Steering Committee, as part of its responsibilities, will evaluate whether they should change their fees to be stated in euros and/or to adjust the fees based on such a change.&nbsp; Should the working group choose to take this action, any change will be communicated in advance of enacting the change.</p>

<p>If you are an individual Committer Member you will be asked to execute the new Belgian membership agreement. There remain no fees for Committer membership.</p>

<p>More information on executing new agreements will follow in the October timeframe.</p>

<p><strong>Q. When do these changes take effect, and what steps do I need to take? </strong></p>

<p>Once the new Belgian based organization is created, expected in early-to-mid October, we will be reaching out to all members to engage with them in supporting this transition.&nbsp; Specifically, we will be asking members to re-execute each of the agreements listed above that your organization has executed previously.&nbsp; We will help you with this process by providing you copies of your existing agreements, and the new ones to be executed.</p>

<p>Concurrent with executing these new versions of the agreements, we will be asking you to execute a letter confirming the resignation of your membership in the existing US-based organization.&nbsp; This formality is required, as that entity will exist for some period of time, and we want to avoid having any organizations paying fees to both the current and the new foundations.</p>

<p><strong>Q. Who is eligible to vote for the changes to the Bylaws and Membership Agreement?&nbsp;</strong></p>

<p>All Committer Members and the Company Representatives of all Strategic and Solutions members are eligible to vote.</p>

<p><strong>Q. Do all Committers get to vote?&nbsp;</strong></p>

<p>Committer Members are Committers who either a) work for a Member Company or b) individuals who have completed the Committer Membership Agreement. Only Committers that are Committer Members are eligible to vote.</p>

<p><strong>Q.&nbsp; How do I vote?</strong></p>

<p>Ballots will be sent electronically by email communication shortly and will contain instructions on how to submit your ballot. &nbsp; A simple yes or no is all that is required, and the changes are voted on as a whole package.&nbsp; A prompt response would be greatly appreciated.</p>

<p><strong>Q. Why should I bother to vote?</strong></p>

<p>These changes cannot be adopted without the approval of our members, and in order to do that we require a quorum of members to vote.&nbsp; It&#39;s extremely important to recognize that your vote is required to enact the changes necessary to stand up the Belgian organization.</p>

<p><strong>Q. How does the voting process work?</strong></p>

<p>Each Company Representative of a Strategic or Solutions member&rsquo;s vote counts as one vote. Each group of committers for a Member company also count as a single vote based on a collapsible single vote. Each vote by an individual Committer who has completed the Committer Membership Agreement and is not working for a Member Company also counts as one vote. The voting process will close at the end of business September 29, 2020 @ 4pm ET. A super-majority of affirmative votes is required for the changes to become effective. The explicit rules for voting, quorum, and eligibility are detailed in Section 6 of the Eclipse Bylaws[1].&nbsp;</p>

<p><strong>Q. I do not see any of the benefits of membership noted in the Membership Agreement or Bylaws; how do I find out about those?&nbsp;</strong></p>

<p>Members all have certain rights and obligations as noted in the Bylaws and Membership Agreement. However, many of the benefits members receive are from programs run by the Eclipse Foundation - thanks to the support of its membership. You can find out about these benefits on the membership benefits page[2] and some of the special programs being run on the special programs page. New benefits and programs are always being added and you can find out about those from your Member Newsletters, Members Meetings, and other information feeds.</p>

<p><strong>Q.&nbsp; I understand there is more information generally available as part of the Eclipse Foundation Inc.&rsquo;s move to Europe? </strong></p>

<p>Yes, the Eclipse Foundation has prepared an in-depth Membership <a href="https://www.eclipse.org/europe/aisbl_membership_faq.php">FAQ</a> regarding its transition to Europe.&nbsp; Additionally the Eclipse Foundation recently held a <a href="https://youtu.be/ucKo_GI3nYI">Town Hall</a> on the matter.&nbsp; You may view the presentation materials <a href="https://drive.google.com/file/d/1i57TCkhVikKpv-3kxE356wZjMBhiJWcx/view?usp=sharing">here</a>.&nbsp;</p>

<p><strong>Q. Who do I contact for more information?&nbsp;</strong></p>

<p>For email questions or to schedule a time to speak to someone directly, please contact <a href="mailto:eclipse-europe@eclipse.org">eclipse-europe@eclipse.org</a>.</p>

<hr />
<p>[1] <a href="https://www.eclipse.org/org/documents/eclipse_foundation-bylaws.pdf">https://www.eclipse.org/org/documents/eclipse_foundation-bylaws.pdf</a></p>

<p>[2] <a href="https://www.eclipse.org/membership/">https://www.eclipse.org/membership/</a></p>
