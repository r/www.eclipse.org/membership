<?php
/**
 * Copyright (c) 2019 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");

$App = new App();
$Nav = new Nav();
$Theme = $App->getThemeClass();
include($App->getProjectCommon());
$Theme->setNav($Nav);

$pageTitle = "Eclipse Bylaw and Membership Agreement Changes 2020";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("eclipse membership, eclipse bylaw changes 2020");
$Theme->setTitle("Eclipse Bylaw Changes 2020");

// Place your html content in a file called content/en_pagename.php
ob_start();
include("content/en_" . $App->getScriptName());
$html = ob_get_clean();
$Theme->setHtml($html);
$Theme->generatePage();